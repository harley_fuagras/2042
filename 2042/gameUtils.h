#pragma once
#include "lib/core.h"
#include <vector>
#include <string>
#include <map>
#include <random>
#include "C_Faction.h"

class Entity;
class IC_AI;

IC_AI*  GetAIComponent(std::string& aiId, Entity* entity);
Entity* NewPlayer();
Entity* NewEnemy(
    vec2 pos, vec2 spawnPos, vec2 size, vec2 velocity, std::vector<std::vector<std::string>>& texPath, 
    std::string& aiId, std::string& stateMachinePath, std::vector<std::string>& weaponIds,
    int points, float health, float collisionDamage
);
Entity* NewBullet(vec2 pos, vec2 velocity, float damage, C_Faction::Type typeFaction);
Entity* NewExplosion(vec2 pos);

bool  CheckRectCollision(vec2 r1Pos, vec2 r1Size, vec2 r2Pos, vec2 r2Size);
int   GetRandInt(int min, int max);
float GetLineHCenterPos(float width, int textLength, float charWidth);
