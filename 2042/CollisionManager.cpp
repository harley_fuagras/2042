#include "stdafx.h"
#include "CollisionManager.h"
#include "Entity.h"
#include "ComponentMessage.h"
#include "C_Collider.h"
#include "GameUtils.h"
#include <algorithm>

void CollisionManager::Run() {
    for (unsigned int i = 0; i < mEnemyColliders.size(); ++i) {
        for (unsigned int j = 0; j < mPlayerColliders.size(); ++j) {
            if (!(mEnemyColliders[i]->GetOwner()->GetType() == Entity::E_BULLET && mPlayerColliders[j]->GetOwner()->GetType() == Entity::E_BULLET)) {
                if (CheckRectCollision(mEnemyColliders[i]->GetPos(), mEnemyColliders[i]->GetSize(), mPlayerColliders[j]->GetPos(), mPlayerColliders[j]->GetSize())) {
                    HurtCMsg hurtMsgFirst = { mEnemyColliders[i]->GetDamage() };
                    mPlayerColliders[j]->GetOwner()->ReceiveMessage(&hurtMsgFirst);

                    HurtCMsg hurtMsgSecond = { mPlayerColliders[j]->GetDamage() };
                    mEnemyColliders[i]->GetOwner()->ReceiveMessage(&hurtMsgSecond);
                }
            }
        }
    }
}

void CollisionManager::Clear() {
    mPlayerColliders.clear();
    mEnemyColliders.clear();
}

C_Faction::Type CollisionManager::GetFaction(C_Collider* collider) {
    GetFactionCMsg getFactionComponentMsg;
    collider->GetOwner()->ReceiveMessage(&getFactionComponentMsg);

    return getFactionComponentMsg.typeFaction;
}


void CollisionManager::AddCollider(C_Collider* collider) {
    C_Faction::Type factionType = GetFaction(collider);
    if (factionType == C_Faction::Type::F_PLAYER) {
        mPlayerColliders.push_back(collider);
    } else if(factionType == C_Faction::Type::F_ENEMY) {
        mEnemyColliders.push_back(collider);
    }
}

void CollisionManager::RemoveCollider(C_Collider* collider) {
    mPlayerColliders.erase(std::remove(mPlayerColliders.begin(), mPlayerColliders.end(), collider), mPlayerColliders.end());
    mEnemyColliders.erase(std::remove(mEnemyColliders.begin(), mEnemyColliders.end(), collider), mEnemyColliders.end());
}
