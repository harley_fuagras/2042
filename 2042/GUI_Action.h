#pragma once
#include "Button.h"
#include "LocalizationManager.h"

class GUI_Action : public Button::IListener {
public:
    virtual void OnPressEnter(Button *p) = 0;

};

class GUI_Action_ChangeMode : public GUI_Action {
public:
    GUI_Action_ChangeMode(AppManager::AppModeId mode)
        : mMode(mode)
    {}
    
    virtual void OnPressEnter(Button *p) override {
        gAppManager->SetWantedModeId(mMode);
        Menu* m = gGraphicEngine->GetMenuState();
        GAME_DELETE(m);
        gGraphicEngine->SetMenuState(nullptr);
    }
private:
    AppManager::AppModeId mMode;
};

class GUI_Action_ChangeMenuState : public GUI_Action {
public:
    GUI_Action_ChangeMenuState(Menu::Type id)
        : mMenuType(id)
    {}

    virtual void OnPressEnter(Button *p) override {
        Menu::Type menuTypeAux = mMenuType;
        Menu* m = gGraphicEngine->GetMenuState();
        GAME_DELETE(m);
        gGraphicEngine->SetMenuState(nullptr);
        Menu* menuState = GAME_NEW(Menu, (menuTypeAux));
    }
private:
    Menu::Type mMenuType;
};

class GUI_Action_ToggleMusic : public GUI_Action {
public:
    virtual void OnPressEnter(Button *p) override {
        gGame->ToggleMusic();
        p->SetValue(gGame->GetIsMusicOn() ? "ON" : "OFF");

        if (!gGame->GetIsMusicOn())
            gSoundManager->StopMusic();
        else {
            std::string menu_music = "menu_music";
            gSoundManager->PlayMusic(menu_music);
        }
    }
};

class GUI_Action_ToggleEffects : public GUI_Action {
public:
    virtual void OnPressEnter(Button *p) override {
        gGame->ToggleSoundFX();
        p->SetValue(gGame->GetIsSoundFxOn() ? "ON" : "OFF");
    }
};


class GUI_Action_SwapLocale : public GUI_Action {
public:
    virtual void OnPressEnter(Button *p) override {
        gLocalizationManager->SwapNextLocale();
        p->SetValue(gLocalizationManager->GetLocaleName());
    }
};

class GUI_Action_LoadLevel : public GUI_Action {
public:
    GUI_Action_LoadLevel(std::string& levelId) {
        mLevelId   = levelId;
    }

    virtual void OnPressEnter(Button *p) override {
        gAppManager->SetWantedOptionId(mLevelId);
        gAppManager->SetWantedModeId(AppManager::AM_LEVEL_TRANSITION);

        Menu* m = gGraphicEngine->GetMenuState();
        GAME_DELETE(m);
        gGraphicEngine->SetMenuState(nullptr);
    }
private:
    std::string mLevelId;
};

class GUI_Action_ContinueGame : public GUI_Action {
public:
    virtual void OnPressEnter(Button *p) override {
        gWorld->TogglePause();
    }
};

class GUI_Action_Exit : public GUI_Action {
public:
    virtual void OnPressEnter(Button *p) override {
        gGame->SetQuit(true);
    }
};

