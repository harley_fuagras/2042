#include "stdafx.h"
#include "EventManager.h"
#include "Globals.h"
#include "Game.h"
#include "World.h"
#include "GraphicEngine.h"
#include "Menu.h"
#include "ComponentMessage.h"
#include "SoundManager.h"

EventManager::EventManager() {
    mCanPressKeys[KEY_MOVE_DOWN]  = true;
    mCanPressKeys[KEY_MOVE_UP]    = true;
    mCanPressKeys[KEY_MOVE_LEFT]  = true;
    mCanPressKeys[KEY_MOVE_RIGHT] = true;
    mCanPressKeys[KEY_ACCEPT]     = true;
    mCanPressKeys[KEY_SHOOT]      = true;
    mCanPressKeys[KEY_PAUSE]      = true;
}

void EventManager::ManageInput() {
    for (auto& listenerToRemove : mListenersToRemove) {
        mListeners.erase(std::remove(mListeners.begin(), mListeners.end(), listenerToRemove), mListeners.end());
    }
    mListenersToRemove.clear();

    for (auto listenerToAdd = mListenersToAdd.cbegin(); listenerToAdd != mListenersToAdd.cend(); ++listenerToAdd) {
        mListeners.push_back(*listenerToAdd);
    }
    mListenersToAdd.clear();

    std::vector<TEvent> eventTypes = {};

    /*Single press keys*/

    if (SYS_KeyPressed(KEY_MOVE_DOWN)) {
        if (mCanPressKeys[KEY_MOVE_DOWN]) {
            eventTypes.push_back(TEvent::E_KEY_DOWN_SINGLE);
            mCanPressKeys[KEY_MOVE_DOWN] = false;
        }
    } else {
        mCanPressKeys[KEY_MOVE_DOWN] = true;
    }

    if (SYS_KeyPressed(KEY_MOVE_UP)) {
        if (mCanPressKeys[KEY_MOVE_UP]) {
            eventTypes.push_back(TEvent::E_KEY_UP_SINGLE);
            mCanPressKeys[KEY_MOVE_UP] = false;
        }
    } else {
        mCanPressKeys[KEY_MOVE_UP] = true;
    }

    if (SYS_KeyPressed(KEY_ACCEPT)) {
        if (mCanPressKeys[KEY_ACCEPT]) {
            eventTypes.push_back(TEvent::E_KEY_ENTER_SINGLE);
            mCanPressKeys[KEY_ACCEPT] = false;
        }
    } else {
        mCanPressKeys[KEY_ACCEPT] = true;
    }

    if (SYS_KeyPressed(KEY_PAUSE)) {
        if (mCanPressKeys[KEY_PAUSE]) {
            eventTypes.push_back(TEvent::E_KEY_ESCAPE_SINGLE);
            mCanPressKeys[KEY_PAUSE] = false;
        }
    } else {
        mCanPressKeys[KEY_PAUSE] = true;
    }

    /*Continous press keys*/

    if (SYS_KeyPressed(KEY_MOVE_DOWN))
        eventTypes.push_back(TEvent::E_KEY_DOWN);
     
    if (SYS_KeyPressed(KEY_MOVE_UP))
        eventTypes.push_back(TEvent::E_KEY_UP);

    if (SYS_KeyPressed(KEY_MOVE_LEFT))
        eventTypes.push_back(TEvent::E_KEY_LEFT);
     
    if (SYS_KeyPressed(KEY_MOVE_RIGHT))
        eventTypes.push_back(TEvent::E_KEY_RIGHT);

    if (SYS_KeyPressed(KEY_SHOOT))
        eventTypes.push_back(TEvent::E_KEY_SPACE);

    if (!eventTypes.empty()) {
        for (TEvent& eventType : eventTypes) {
            EventManager::CEvent event = EventManager::CEvent(eventType);

            for (Listener* listener : mListeners) {
                listener->ProcessEvent(event);
            }
        }
    }
}

void EventManager::Register(Listener* listener) {
    mListenersToAdd.push_back(listener);
}

void EventManager::Unregister(Listener* listener) {
    mListenersToRemove.push_back(listener);
}

void EventManager::Clear() {
    for (Listener* listener : mListeners)
        Unregister(listener);
}
