#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "World.h"
#include "AppManagement/AppManager.h"
#include "Graph/TextureManager.h"
#include "LocalizationManager.h"
#include "Graph/GraphicEngine.h"
#include "EventManager.h"
#include "CollisionManager.h"
#include "Sound/SoundManager.h"

void DisplayInit() {
    FONT_Init();
    CORE_InitSound();
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT); // Sets up clipping
    glClearColor(0.0f, 0.1f, 0.3f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, SCR_WIDTH, 0.0, SCR_HEIGHT, 0.0, 1.0);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void DisplayEnd() {
    FONT_End();
    CORE_EndSound();
}

World* gWorld                             = nullptr;
Game* gGame                               = GAME_NEW(Game, ());
AppManager* gAppManager                   = GAME_NEW(AppManager, ());
TextureManager* gTextureManager           = GAME_NEW(TextureManager, ());
LocalizationManager* gLocalizationManager = GAME_NEW(LocalizationManager, (0));
GraphicEngine* gGraphicEngine             = GAME_NEW(GraphicEngine, ());
EventManager* gEventManager               = GAME_NEW(EventManager, ());
CollisionManager* gCollisionManager       = GAME_NEW(CollisionManager, ());
SoundManager* gSoundManager               = GAME_NEW(SoundManager, ());

int Main(void) {
    DisplayInit();
    gAppManager->SwitchToMode(AppManager::AM_MENU);
    gAppManager->SetWantedModeId(AppManager::AM_MENU);
    
    while (!SYS_GottaQuit() && !gGame->GetQuit()) {
        gAppManager->ManageModes();
        gAppManager->ProcessInput();
        gAppManager->Run();
        gCollisionManager->Run();
        gAppManager->Render();

        // Keep system running
        SYS_Pump();
        SYS_Sleep(SLEEP_TIME);
    }
    
    gTextureManager->UnloadAll();
    gSoundManager->UnloadAll();

    GAME_DELETE(gGame);
    GAME_DELETE(gAppManager);
    GAME_DELETE(gTextureManager);
    GAME_DELETE(gLocalizationManager);
    GAME_DELETE(gGraphicEngine);
    GAME_DELETE(gEventManager);
    GAME_DELETE(gCollisionManager);
    GAME_DELETE(gSoundManager);
    GAME_DUMP_LEAKS;

    DisplayEnd();
    return 0;
}
