#pragma once
#include "Globals.h"
#include <map>

class LocalizationManager {
public:
    LocalizationManager(int localeIndex);

    void              LoadTexts(int localeIndex);
    const std::string GetString(const std::string& id) const;
    void              SwapNextLocale();
    const std::string GetLocaleName() const;
    const int         GetLocaleIndex() const;
    void              SetLocaleIndex(int localeIndex);

private:
    std::map<std::string, std::string>                     mTexts;
    unsigned int                                           mLocaleIndex;
    const std::vector<std::pair<std::string, std::string>> mLocales = LOCALES;

};
