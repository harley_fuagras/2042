#pragma once
#include <vector>
#include <map>

class EventManager {
public:
    enum class TEvent {
        E_NONE,
        E_KEY_UP,
        E_KEY_LEFT,
        E_KEY_DOWN,
        E_KEY_RIGHT,
        E_KEY_SPACE,
        E_KEY_ESCAPE,
        E_KEY_ENTER,

        E_KEY_UP_SINGLE,
        E_KEY_DOWN_SINGLE,
        E_KEY_ENTER_SINGLE,
        E_KEY_ESCAPE_SINGLE,
    };

    class CEvent {
    public:
        CEvent(TEvent eventType) : m_type(eventType)
        {};
        TEvent GetType() const { return m_type; }
    private:
        TEvent m_type;
    };

    class Listener {
    public:
        virtual ~Listener() {}
        virtual bool ProcessEvent(const EventManager::CEvent& e) = 0;
    };
    
    EventManager();
    void ManageInput();

    void Register(Listener* listener);
    void Unregister(Listener* listener);
    void Clear();

private:
    std::map<int, bool>    mCanPressKeys;
    std::vector<Listener*> mListeners;
    std::vector<Listener*> mListenersToAdd;
    std::vector<Listener*> mListenersToRemove;

};
