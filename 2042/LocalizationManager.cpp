#include "stdafx.h"
#include "LocalizationManager.h"
#include "Globals.h"
#include "lib/rapidjson/document.h"
#include "lib/rapidjson/filereadstream.h"
#include "lib/rapidjson/stringbuffer.h"

LocalizationManager::LocalizationManager(int localeIndex) {
    mLocaleIndex = localeIndex;
    LoadTexts(mLocaleIndex);
}

void LocalizationManager::LoadTexts(int localeIndex) {
    mLocaleIndex = localeIndex;
    mTexts.clear();

    FILE* fp;
    fopen_s(&fp, (LOCALIZATION_BASE_PATH + mLocales[mLocaleIndex].first + ".json").data(), "rb");

    char readBuffer[65536];
    rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
    rapidjson::Document d;
    d.ParseStream(is);
    fclose(fp);

    for (auto& m : d.GetObject())
        mTexts.insert(std::pair<std::string, std::string>(m.name.GetString(), m.value.GetString()));
}

const std::string LocalizationManager::GetString(const std::string& id) const {
    return mTexts.at(id);
}

const int LocalizationManager::GetLocaleIndex() const {
    return mLocaleIndex;
}

void LocalizationManager::SetLocaleIndex(int localeIndex) {
    mLocaleIndex = localeIndex;
}

const std::string LocalizationManager::GetLocaleName() const {
    return mLocales[mLocaleIndex].second;
}

void LocalizationManager::SwapNextLocale() {
    if (mLocaleIndex >= mLocales.size() - 1)
        mLocaleIndex = 0;
    else
        mLocaleIndex++;
    LoadTexts(mLocaleIndex);
}
