#pragma once
#include "lib/core.h"
#include <string>
#include <map>

class SoundManager {
public:
	ALuint LoadSound(std::string& soundPath, std::string& soundKey);
	void   UnloadSound(ALuint sound);
	void   UnloadAll();
	void   PlaySound(std::string& soundKey, float volume = 1.0f, float pitch = 1.0f);
	void   PlayMusic(std::string& soundKey, float volume = 1.0f);
	void   StopMusic();

private:
	std::map<std::string, GLuint> mLoadedSounds;

};
