#include "stdafx.h"
#include "Globals.h"
#include "lib/core.h"
#include "SoundManager.h"
#include "Game.h"

ALuint SoundManager::LoadSound(std::string& soundPath, std::string& soundKey) {
    if (mLoadedSounds.find(soundKey) == mLoadedSounds.end()) {
        ALuint sound = CORE_LoadWav(soundPath.data());
        CUSTOM_ASSERT_FILE(sound, "Sound file", soundPath);
        mLoadedSounds[soundKey] = sound;
        return sound;
    }
    else {
        return mLoadedSounds[soundKey];
    }
}

void SoundManager::UnloadSound(ALuint sound) {
    std::string id = "";
    for (auto it = mLoadedSounds.begin(); it != mLoadedSounds.end(); ++it)
        if (it->second == sound)
            id = it->first;
    mLoadedSounds.erase(id);
    CORE_UnloadWav(sound);
}

void SoundManager::UnloadAll() {
    for (auto it = mLoadedSounds.begin(); it != mLoadedSounds.end(); ++it)
        CORE_UnloadWav(it->second);
    mLoadedSounds.clear();
}

void SoundManager::PlaySound(std::string& soundKey, float volume, float pitch) {
    if(gGame->GetIsSoundFxOn())
        CORE_PlaySound(mLoadedSounds[soundKey], volume, pitch);
}

void SoundManager::PlayMusic(std::string& soundKey, float volume) {
    if (gGame->GetIsMusicOn())
        CORE_PlayMusic(mLoadedSounds[soundKey], volume);
}

void SoundManager::StopMusic() {
    CORE_StopMusic();
}
