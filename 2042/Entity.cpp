#include "stdafx.h"
#include "Globals.h"
#include "GraphicEngine.h"
#include "Entity.h"
#include "Component.h"
#include "Sprite.h"
#include "ComponentMessage.h"
#include "CollisionManager.h"
#include <algorithm>

Entity::Entity(vec2 pos, vec2 size, Type type)
    : mPos(pos), mSize(size), mType(type)
{}

Entity::~Entity() {
    // Remove collider component from Collision Manager.
    GetColliderCMsg getColliderMsg;
    ReceiveMessage(&getColliderMsg);
    if(getColliderMsg.collider)
        gCollisionManager->RemoveCollider(getColliderMsg.collider);

    for (auto& component : mComponents) {
        GAME_DELETE(component);
        component = nullptr;
    }
    mComponents.clear();

    for (auto& component : mComponentsToAdd) {
        GAME_DELETE(component);
        component = nullptr;
    }
    mComponentsToAdd.clear();
}

void Entity::Run() {
    for (auto compIt = mComponents.begin(); compIt != mComponents.end(); ++compIt)
        (*compIt)->Run();
}

void Entity::AddComponent(Component* component) {
    mComponents.push_back(component);
}

void Entity::ReceiveMessage(ComponentMessage* msg) {
    for (auto compIt = mComponents.begin(); compIt != mComponents.end(); ++compIt)
        (*compIt)->ReceiveMessage(msg);
}

void Entity::SetPos(vec2 pos) {
    mPos = pos;
}

void Entity::SetSize(vec2 size) {
    mSize = size;
}

vec2 Entity::GetPos() const {
    return mPos;
}

vec2 Entity::GetSize() const {
    return mSize;
}

Entity::Type Entity::GetType() const {
    return mType;
}
