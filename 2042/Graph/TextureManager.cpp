#include "stdafx.h"
#include "Globals.h"
#include "lib/core.h"
#include "TextureManager.h"

GLuint TextureManager::LoadTexture(std::string& texPath, bool wrap) {
    if (mLoadedTextures.find(texPath) == mLoadedTextures.end()) {
        GLuint gfx = CORE_LoadPNG(texPath.data(), wrap);
        CUSTOM_ASSERT_FILE(gfx, "Texture file", texPath);
        mLoadedTextures[texPath] = gfx;
        return gfx;
    } else
        return mLoadedTextures[texPath];
}

void TextureManager::UnloadTexture(GLuint gfx) {
    std::string id = "";
    for (auto it = mLoadedTextures.begin(); it != mLoadedTextures.end(); ++it)
        if (it->second == gfx)
            id = it->first;
    mLoadedTextures.erase(id);
    CORE_UnloadPNG(gfx);
}

void TextureManager::UnloadAll() {
    for (auto it = mLoadedTextures.begin(); it != mLoadedTextures.end(); ++it)
        CORE_UnloadPNG(it->second);
    mLoadedTextures.clear();
}
