#pragma once

class Sprite;
class Background;
class Menu;
class TextBox;

class GraphicEngine {
public:
    GraphicEngine();
    ~GraphicEngine();
    
    void Render();
    void Clear(std::vector<Sprite*> spriteExceptions = {}, std::vector<Background*> bgExceptions = {}, std::vector<TextBox*> tbExceptions = {});
    void ClearSprites(std::vector<Sprite*> exceptions = {});
    void ClearBackgrounds(std::vector<Background*> exceptions = {});
    void ClearTextBoxes(std::vector<TextBox*> exceptions = {});
    void SetWillClearMenuState(bool willClearMenuState);
    void ClearMenuState();
    void AddSprite(Sprite* sprite);
    void RemoveSprite(Sprite* sprite);
    void AddBackground(Background* background);
    void AddTextBox(TextBox* textBox);
    void RemoveTextBox(TextBox* textBox);

    Menu* GetMenuState() const;
    void  SetMenuState(Menu* menuState);

private:
    std::vector<Sprite*>     mSprites;
    std::vector<Background*> mBackgrounds;
    Menu*                    mMenuState;
    std::vector<TextBox*>    mTextBoxes;
    bool                     mWillClearMenuState;

};
