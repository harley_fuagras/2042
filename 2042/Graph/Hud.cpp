#include "stdafx.h"
#include "GraphicEngine.h"
#include "Hud.h"
#include "TextBox.h"
#include "LocalizationManager.h"
#include <cmath>

Hud::Hud(int score, int playerLifes) {
    std::string playerLifesText = std::to_string(playerLifes);
    std::string scoreText       = std::to_string(score);

    std::string playerLifesHudText = gLocalizationManager->GetString("hud_player_lifes") + ": " + playerLifesText;
    std::string scoreHudText       = gLocalizationManager->GetString("hud_score") + ": " + std::string(SCORE_ZEROES_PAD - scoreText.length(), '0').append(scoreText);
    
    float posY = HUD_POS_Y;
    mPlayerLifesTextBox = GAME_NEW(TextBox, (playerLifesHudText, HUD_TEXT_COLOR, vmake(HUD_MARGIN_H, posY)));

    float scoreHudPosX = static_cast<float>(SCR_WIDTH - (scoreHudText.length() * HUD_CHAR_WIDTH) - HUD_MARGIN_H);
    mScoreTextBox = GAME_NEW(TextBox, (scoreHudText, HUD_TEXT_COLOR, vmake(scoreHudPosX, posY)));

    gGraphicEngine->AddTextBox(mPlayerLifesTextBox);
    gGraphicEngine->AddTextBox(mScoreTextBox);
}

Hud::~Hud() {
}

void Hud::UpdateScoreText(int score) {
    std::string scoreText    = std::to_string(score);
    std::string scoreTextHud = gLocalizationManager->GetString("hud_score") + ": " + std::string(SCORE_ZEROES_PAD - scoreText.length(), '0').append(scoreText);
    mScoreTextBox->SetText(scoreTextHud);
}

void Hud::UpdatePlayerLifesText(int playerLifes) {
    std::string playerLifesText = std::to_string(playerLifes);
    std::string playerLifesTextHud = gLocalizationManager->GetString("hud_player_lifes") + ": " + playerLifesText;
    mPlayerLifesTextBox->SetText(playerLifesTextHud);
}
