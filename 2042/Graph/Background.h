#pragma once
#include "lib/core.h"
#include <vector>

class Background {
public:
    Background(vec2 size, vec2 pos, std::string& texPath, vec2 scrollRatio);
    ~Background();

    void Render();
    void Run();
    bool isStill() const;

    void   SetGfx(GLuint gfx);
    void   SetScrollRatio(vec2 scrollRatio);
    GLuint GetGfx() const;
    vec2   GetScrollRatio() const;

protected:
    vec2   mSize;
    vec2   mPos;
    GLuint mGfx;
    vec2   mScrollRatio;

};
