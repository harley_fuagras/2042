#pragma once
#include "lib/core.h"
#include <string>

class TextBox {
public:
    TextBox(std::string& text, rgba_t color, vec2 pos);

    void SetText(std::string& text);
    
    void Render();

private:
    std::string mText;
    rgba_t      mColor;
    vec2        mPos;

};
