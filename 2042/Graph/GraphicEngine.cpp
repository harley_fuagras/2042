#include "stdafx.h"
#include "Globals.h"
#include "GraphicEngine.h"
#include "Background.h"
#include "Sprite.h"
#include "TextBox.h"
#include "lib/sys.h"
#include "Menu.h"
#include <algorithm>

GraphicEngine::GraphicEngine() {
    mMenuState = nullptr;
}

GraphicEngine::~GraphicEngine() {
    Clear();
}

void GraphicEngine::AddSprite(Sprite* sprite) {
    mSprites.push_back(sprite);
}

void GraphicEngine::RemoveSprite(Sprite* sprite) {
    mSprites.erase(std::remove(mSprites.begin(), mSprites.end(), sprite), mSprites.end());
}

void GraphicEngine::AddBackground(Background* background) {
    mBackgrounds.push_back(background);
}

void GraphicEngine::AddTextBox(TextBox* textBox) {
    mTextBoxes.push_back(textBox);
}

void GraphicEngine::RemoveTextBox(TextBox* textBox) {
    mTextBoxes.erase(std::remove(mTextBoxes.begin(), mTextBoxes.end(), textBox), mTextBoxes.end());
}

Menu* GraphicEngine::GetMenuState() const {
    return mMenuState;
}

void GraphicEngine::SetMenuState(Menu* menuState) {
    mMenuState = menuState;
}

void GraphicEngine::Render() {

    //Check if mMenuState has to be cleared.
    if (mWillClearMenuState)
        ClearMenuState();

    glClear(GL_COLOR_BUFFER_BIT);

    // Render backgrounds.
    for (auto& background : mBackgrounds)
        background->Render();
    
    // Render sprites.
    int index = mSprites.size() - 1;
    for (auto rit = mSprites.rbegin(); rit != mSprites.rend(); ++rit, --index) {
        if((*rit)->GetIsVisible())
            (*rit)->Render();
    }
    
    // Render text boxes.
    for (auto& textBox : mTextBoxes)
        textBox->Render();
    
    // Render menu if exists.
    if (mMenuState)
        mMenuState->Render();

    SYS_Show();
}

void GraphicEngine::Clear(std::vector<Sprite*> spriteExceptions, std::vector<Background*> bgExceptions, std::vector<TextBox*> tbExceptions) {
    ClearSprites(spriteExceptions);
    ClearBackgrounds(bgExceptions);
    ClearTextBoxes(tbExceptions);
    ClearMenuState();
}

void GraphicEngine::ClearSprites(std::vector<Sprite*> exceptions) {
    auto sprite = mSprites.begin();
    for (sprite; sprite != mSprites.cend(); ) {
        if (std::find(exceptions.begin(), exceptions.end(), *sprite) == exceptions.end())
            sprite = mSprites.erase(sprite);
        else
            sprite++;
    }
}

void GraphicEngine::ClearBackgrounds(std::vector<Background*> exceptions) {
    for (auto& background : mBackgrounds) {
        if (std::find(exceptions.begin(), exceptions.end(), background) == exceptions.end()) {
            mBackgrounds.pop_back();
            GAME_DELETE(background);
            background = nullptr;
        }
    }
}

void GraphicEngine::ClearTextBoxes(std::vector<TextBox*> exceptions) {
    for (auto& textBox : mTextBoxes) {
        if (std::find(exceptions.begin(), exceptions.end(), textBox) == exceptions.end()) {
            mTextBoxes.pop_back();
            GAME_DELETE(textBox);
            textBox = nullptr;
        }
    }
}

void GraphicEngine::ClearMenuState() {
    GAME_DELETE(mMenuState);
    mMenuState = nullptr;
}

void GraphicEngine::SetWillClearMenuState(bool willClearMenuState) {
    mWillClearMenuState = willClearMenuState;
}
