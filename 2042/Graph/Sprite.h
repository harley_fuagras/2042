#pragma once
#include "lib/core.h"
#include <vector>
#include "Entity.h"

class Sprite {
public:
    Sprite(Entity* owner, std::vector<std::vector<std::string>>& texPath, int animFps, bool loop, bool selfDestruct);

    void Render();

    int  GetCurrentGfxRow() const;
    void SetCurrentGfxRow(int currentGfxRow);
    
    void SetIsVisible(bool isVisible);
    bool GetIsVisible() const;

private:
    Entity*                          mOwner;
    std::vector<std::vector<GLuint>> mGfx;
    unsigned int                     mAnimFps; //Ticks.
    unsigned int                     mCurrentGfxCol;
    unsigned int                     mCurrentGfxRow;
    unsigned int                     mTicksCount;
    bool                             mLoop;
    bool                             mSelfDestruct; //At ending first animation loop.
    bool                             mIsVisible;

};
