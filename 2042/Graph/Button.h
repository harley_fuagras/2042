#pragma once
#include "Game.h"
#include "World.h"
#include "LocalizationManager.h"
#include "GraphicEngine.h"
#include "Menu.h"
#include "AppMode.h"

class Button {
public:
    class IListener {
    public:
        virtual void OnPressEnter(Button *p) = 0;
    };

    Button(std::string& text, std::string value = "", bool isActive = false)
        : mText(text), mIsActive(isActive), mValue(value)
    {};

    ~Button() {
        GAME_DELETE(mListener);
    }

    std::string GetText() const {
        return mText;
    };

    std::string GetValue() const {
        return mValue;
    };

    IListener* GetListener() const {
        return mListener;
    };

    void SetText(const std::string& text) {
        mText = text;
    };

    void SetValue(const std::string& value) {
        mValue = value;
    };

    void SetListener(IListener* listener) {
        mListener = listener;
    }

protected:
    std::string mText;
    std::string mValue;
    bool        mIsActive;
    IListener*  mListener;

};
