#include "stdafx.h"
#include "Globals.h"
#include "Sprite.h"
#include "Game.h"
#include "TextureManager.h"
#include "ComponentMessage.h"

Sprite::Sprite(Entity* owner, std::vector<std::vector<std::string>>& texPath, int animFps, bool loop, bool selfDestruct)
    : mOwner(owner), mCurrentGfxRow(0), mCurrentGfxCol(0), mTicksCount(0), mAnimFps(animFps), mLoop(loop), mSelfDestruct(selfDestruct), mIsVisible(true)
{
    for (unsigned int i = 0; i < texPath.size(); i++) {
        mGfx.push_back(std::vector<GLuint>());
        for (unsigned int j = 0; j < texPath[i].size(); j++) {
            mGfx[i].push_back(gTextureManager->LoadTexture(texPath[i][j], true));
        }
    }
}

int Sprite::GetCurrentGfxRow() const {
    return mCurrentGfxRow;
}

void Sprite::SetCurrentGfxRow(int currentGfxRow) {
    mCurrentGfxRow = currentGfxRow;
}

void Sprite::SetIsVisible(bool isVisible) {
    mIsVisible = isVisible;
}

bool Sprite::GetIsVisible() const {
    return mIsVisible;
}

void Sprite::Render() {
    if (mTicksCount >= mAnimFps) {
        mCurrentGfxCol++;
        if (mCurrentGfxCol + 1 > mGfx[0].size()) {
            if (mLoop) {
                mCurrentGfxCol = 0;
            } else {
                mCurrentGfxCol = mGfx[0].size() - 1;
                if (mSelfDestruct) {
                    SetWillBeDestroyedCMsg setWillBeDestroyed = { true, false };
                    mOwner->ReceiveMessage(&setWillBeDestroyed);
                }
            }
        }
        mTicksCount = 0;
    }
    mTicksCount++;

    CORE_RenderCenteredSprite(mOwner->GetPos(), mOwner->GetSize(), mGfx[mCurrentGfxRow][mCurrentGfxCol]);
}
