#pragma once
#include "Globals.h"

class Hud {
public:
    Hud(int score, int playerLifes);
    ~Hud();
    
    void UpdateScoreText(int score);
    void UpdatePlayerLifesText(int playerLifes);

private:
    TextBox* mPlayerLifesTextBox;
    TextBox* mScoreTextBox;

};
