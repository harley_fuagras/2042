#include "stdafx.h"
#include "TextBox.h"
#include "lib/font.h"
#include "GameUtils.h"
#include "Globals.h"

TextBox::TextBox(std::string& text, rgba_t color, vec2 pos)
    : mText(text), mColor(color), mPos(pos)
{}

void TextBox::SetText(std::string& text) {
    mText = text;
}

void TextBox::Render() {
    FONT_DrawString(mPos, mText.data(), mColor);
}
