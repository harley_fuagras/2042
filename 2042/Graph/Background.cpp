#include "stdafx.h"
#include "Globals.h"
#include "Background.h"
#include "Game.h"
#include "World.h"
#include "Sprite.h"
#include "TextureManager.h"

Background::Background(vec2 size, vec2 pos, std::string& texPath, vec2 scrollRatio) :
    mSize(size), mPos(pos), mScrollRatio(scrollRatio)
{
    std::string bgPath = texPath.data();
    mGfx = gTextureManager->LoadTexture(bgPath, false);
}

Background::~Background() {
}

void Background::Render() {
    if (!isStill()) {
        vec2 p0 = vsub(mPos, vmul(gWorld->GetCameraPos(), mScrollRatio));
        vec2 p1 = vsub(vadd(mPos, mSize), vmul(gWorld->GetCameraPos(), mScrollRatio));
        CORE_RenderSprite(p0, p1, mGfx);
    } else {
        CORE_RenderCenteredSprite(mPos, mSize, mGfx);
    }	
}

// Check if camera is no longer looking at this background to repos it.
// (For infinite backgrounds).
void Background::Run() {
    float cameraBgBottomPos = gWorld->GetCameraPos().y * mScrollRatio.y;
    if (cameraBgBottomPos >= mPos.y + mSize.y)
        mPos = vmake(0, mPos.y + mSize.y * 2); //We place it above of the upper background.
}

void Background::SetGfx(GLuint gfx) {
    mGfx = gfx;
}

void Background::SetScrollRatio(vec2 scrollRatio) {
    mScrollRatio = scrollRatio;
}

GLuint Background::GetGfx() const {
    return mGfx;
}

vec2 Background::GetScrollRatio() const {
    return mScrollRatio;
}

bool Background::isStill() const {
    return(mScrollRatio.x == 0.0f && mScrollRatio.y == 0.0f);
}
