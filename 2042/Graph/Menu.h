#pragma once
#include "lib/core.h"
#include <vector>
#include "EventManager.h"

class Button;

class Menu : public EventManager::Listener {
public:
    enum Type {
        MS_NULL,
        MS_MAIN_MENU,
        MS_NEW_GAME,
        MS_OPTIONS,
        MS_PAUSE,
        MS_GAMEOVER,
    };

    Menu(Type id);
    ~Menu();
    void Render();

    std::vector<Button*>* GetButtons();
    int GetCurrentButtonIndex() const;
    Type GetTypeId() const;

    void SetCurrentButtonIndex(int buttonIndex);

    virtual bool ProcessEvent(const EventManager::CEvent& e);
    void ActivateNextButton();
    void ActivatePrevButton();
    void ExecuteButton();

private:
    std::string          mTitle;
    float                mTitleMargin;
    std::vector<Button*> mButtons;
    int                  mCurrentButtonIndex;
    float                mPosY;
    float                mButtonMargin;
    rgba_t               mButtonColor;
    rgba_t               mButtonActiveColor;
    Type                 mTypeId;

};
