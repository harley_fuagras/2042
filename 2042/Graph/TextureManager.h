#pragma once
#include <string>
#include <map>

class TextureManager {
public:
    GLuint LoadTexture(std::string& texPath, bool wrap);
    
    void UnloadTexture(GLuint gfx);
    void UnloadAll();

private:
    std::map<std::string, GLuint> mLoadedTextures;

};
