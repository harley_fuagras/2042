#include "stdafx.h"
#include "Menu.h"
#include "Button.h"
#include "GUI_Action.h"
#include "lib/font.h"

Menu::Menu(Type id) {

    switch (id) {
        case MS_MAIN_MENU: {
            mTitle = "menu_main";
            std::string b1Key = "btn_new_game";
            std::string b2Key = "btn_options";
            std::string b3Key = "btn_exit";
        
            Button* b1 = GAME_NEW(Button, (b1Key));
            mButtons.push_back(b1);
            GUI_Action* a1 = GAME_NEW(GUI_Action_ChangeMenuState, (MS_NEW_GAME));
            b1->SetListener(a1);

            Button* b2 = GAME_NEW(Button, (b2Key));
            mButtons.push_back(b2);
            GUI_Action* a2 = GAME_NEW(GUI_Action_ChangeMenuState, (MS_OPTIONS));
            b2->SetListener(a2);

            Button* b3 = GAME_NEW(Button, (b3Key));
            mButtons.push_back(b3);
            GUI_Action* a3 = GAME_NEW(GUI_Action_Exit, ());
            b3->SetListener(a3);

            mPosY = MENU_POS_Y_MAIN;
            break;
        }
        case MS_NEW_GAME: {
            mTitle = "menu_new_game";
            std::map<std::string, Game::LevelData> gameLevelsData = gGame->GetLevelsData();
            for (auto& level : gameLevelsData) {
                std::string bLevelText = "level";
                std::string bLevelId = level.second.id;
                std::string bLevelKey = gLocalizationManager->GetString(level.second.levelName);
            
                Button* b = GAME_NEW(Button, (bLevelText, bLevelKey));
                mButtons.push_back(b);
                GUI_Action* a = GAME_NEW(GUI_Action_LoadLevel, (bLevelId));
                b->SetListener(a);
            }

            std::string b1_key = "btn_back";
            Button* b1 = GAME_NEW(Button, (b1_key));
            mButtons.push_back(b1);
            GUI_Action* a1 = GAME_NEW(GUI_Action_ChangeMenuState, (MS_MAIN_MENU));
            b1->SetListener(a1);

            mPosY = MENU_POS_Y_MAIN;
            break;
        }
        case MS_OPTIONS: {
            mTitle = "menu_options";
            std::string b1Key = "btn_music";
            std::string b2Key = "btn_effects";
            std::string b3Key = "btn_language";
            std::string b4Key = "btn_back";

            Button* b1 = GAME_NEW(Button, (b1Key, gGame->GetIsMusicOn() ? "ON" : "OFF"));
            mButtons.push_back(b1);
            GUI_Action* a1 = GAME_NEW(GUI_Action_ToggleMusic, ());
            b1->SetListener(a1);
        
            Button* b2 = GAME_NEW(Button, (b2Key, gGame->GetIsSoundFxOn() ? "ON" : "OFF"));
            mButtons.push_back(b2);
            GUI_Action* a2 = GAME_NEW(GUI_Action_ToggleEffects, ());
            b2->SetListener(a2);
        
            Button* b3 = GAME_NEW(Button, (b3Key, gLocalizationManager->GetLocaleName()));
            mButtons.push_back(b3);
            GUI_Action* a3 = GAME_NEW(GUI_Action_SwapLocale, ());
            b3->SetListener(a3);

            Button* b4 = GAME_NEW(Button, (b4Key));
            mButtons.push_back(b4);
            GUI_Action* a4 = GAME_NEW(GUI_Action_ChangeMenuState, (MS_MAIN_MENU));
            b4->SetListener(a4);

            mPosY = MENU_POS_Y_MAIN;
            break;
        }
        case MS_PAUSE: {
            mTitle = "menu_pause";
            std::string b1Key = "btn_continue";
            std::string b2Key = "btn_abandon";

            Button* b1 = GAME_NEW(Button, (b1Key));
            mButtons.push_back(b1);
            GUI_Action* a1 = GAME_NEW(GUI_Action_ContinueGame, ());
            b1->SetListener(a1);

            Button* b2 = GAME_NEW(Button, (b2Key));
            mButtons.push_back(b2);
            GUI_Action* a2 = GAME_NEW(GUI_Action_ChangeMode, (AppManager::AM_MENU));
            b2->SetListener(a2);
            mPosY = MENU_POS_Y;
            break;
        }
        case MS_GAMEOVER: {
            mTitle = "menu_gameover";
            std::string b1LevelText = "level";
            std::string b2Key = "btn_abandon";

            std::map<std::string, Game::LevelData> gameLevelsData = gGame->GetLevelsData();
            std::string b1LevelId = gameLevelsData.begin()->first;
            std::string bLevelKey = gLocalizationManager->GetString(gameLevelsData.begin()->second.levelName);

            Button* b1 = GAME_NEW(Button, (b1LevelText, bLevelKey));
            mButtons.push_back(b1);
            GUI_Action* a1 = GAME_NEW(GUI_Action_LoadLevel, (b1LevelId));
            b1->SetListener(a1);
        
            Button* b2 = GAME_NEW(Button, (b2Key));
            mButtons.push_back(b2);
            GUI_Action* a2 = GAME_NEW(GUI_Action_ChangeMode, (AppManager::AM_MENU));
            b2->SetListener(a2);

            mPosY = MENU_POS_Y;
            break;
        }
    }

    mTypeId = id;
    mButtonMargin = MENU_BTN_MARGIN;
    mTitleMargin = MENU_TITLE_MARGIN;
    mButtonColor = MENU_TITLE_COLOR;
    mButtonColor = MENU_BTN_COLOR;
    mButtonActiveColor = MENU_BTN_COLOR_ACTIVE;
    mCurrentButtonIndex = 0;

    gGraphicEngine->SetMenuState(this);
    gGraphicEngine->SetWillClearMenuState(false);
    gEventManager->Register(this);
}

Menu::~Menu() {
    for (auto& button : mButtons) {
        GAME_DELETE(button);
        button = nullptr;
    }
    gGraphicEngine->SetMenuState(nullptr);
    gEventManager->Unregister(this);
}

void Menu::Render() {
    float drawPosY = mPosY;

    // Render title.
    std::string translatedTitle = gLocalizationManager->GetString(mTitle);
    float drawPosX              = GetLineHCenterPos(SCR_WIDTH, translatedTitle.length(), MENU_CHAR_WIDTH);

    FONT_DrawString(vmake(drawPosX, drawPosY), translatedTitle.data(), MENU_TITLE_COLOR);
    drawPosY = drawPosY - mTitleMargin;

    int id = 0;
    // Render buttons.
    for (auto& button : *GetButtons()) {
        rgba_t color     = GetCurrentButtonIndex() == id ? mButtonActiveColor : mButtonColor;
        std::string text = gLocalizationManager->GetString((*button).GetText());
        
        if((*button).GetValue() != "")
            text += " " + (*button).GetValue();
        
        float drawPosX = GetLineHCenterPos(SCR_WIDTH, text.length(), MENU_CHAR_WIDTH);

        FONT_DrawString(vmake(drawPosX, drawPosY), text.data(), color);
        drawPosY = drawPosY - mButtonMargin;
        id++;
    }
}

std::vector<Button*>* Menu::GetButtons() {
    return &mButtons;
}

int Menu::GetCurrentButtonIndex() const {
    return mCurrentButtonIndex;
}

Menu::Type Menu::GetTypeId() const {
    return mTypeId;
}

void Menu::SetCurrentButtonIndex(int buttonIndex) {
    mCurrentButtonIndex = buttonIndex;
}

bool Menu::ProcessEvent(const EventManager::CEvent& e) {
    EventManager::TEvent eventType = e.GetType();

    switch (eventType) {
        case EventManager::TEvent::E_KEY_UP_SINGLE: {
            ActivatePrevButton();
            std::string selectFxKey = FX_SELECT_KEY;
            gSoundManager->PlaySound(selectFxKey);
            return true;
        }
        case EventManager::TEvent::E_KEY_DOWN_SINGLE: {
            ActivateNextButton();
            std::string selectFxKey = FX_SELECT_KEY;
            gSoundManager->PlaySound(selectFxKey);
            return true;
        }
        case EventManager::TEvent::E_KEY_ENTER_SINGLE: {
            ExecuteButton();
            return true;
        }
    }
    return false;
}

void Menu::ActivateNextButton() {
    if (mCurrentButtonIndex + 1 == mButtons.size())
        mCurrentButtonIndex = 0;
    else
        mCurrentButtonIndex++;
}

void Menu::ActivatePrevButton() {
    if (mCurrentButtonIndex == 0)
        mCurrentButtonIndex = mButtons.size() - 1;
    else
        mCurrentButtonIndex--;
}

void Menu::ExecuteButton() {
    mButtons[mCurrentButtonIndex]->GetListener()->OnPressEnter(mButtons[mCurrentButtonIndex]);
}
