#include "stdafx.h"
#include "Globals.h"

#if ENABLE_GAME_ASSERT > 0
#include <stdio.h>
#include <windows.h>
#include <cstdlib>

void Assert(bool exp, std::string msg, const char *eFileName, unsigned int lineNum) {
	char buf[1024];
	if (!exp) {
		sprintf_s(buf, "%s(%d) %s\n", eFileName, lineNum, msg.data());
		OutputDebugStringA(buf);
	}
	assert(exp);
}

void AssertFile(bool exp, std::string msg, std::string fileName, const char *eFileName, unsigned int lineNum) {
	char buf[1024];
	if (!exp) {
		sprintf_s(buf, "%s(%d) %s %s could not be found.\n", eFileName, lineNum, msg.data(), fileName.data());
		OutputDebugStringA(buf);
	}
	assert(exp);
}

#endif
