#pragma once
#include <string>

#if ENABLE_GAME_ASSERT > 0

#define CUSTOM_ASSERT(exp, msg) Assert(exp, msg, __FILE__, __LINE__)
#define CUSTOM_ASSERT_FILE(exp, msg, fileName) AssertFile(exp, msg, fileName, __FILE__, __LINE__)

void Assert(bool exp, std::string msg, const char *eFileName, unsigned int lineNum);
void AssertFile(bool exp, std::string msg, std::string fileName, const char *eFileName, unsigned int lineNum);

#else

#define CUSTOM_ASSERT(exp, msg)
#define CUSTOM_ASSERT_FILE(exp, msg, fileName)

#endif
