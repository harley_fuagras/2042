#pragma once

#if ENABLE_LEAK_CONTROL > 0

#define GAME_NEW(className, params) ((className *)AddTrack((unsigned int)new className params, sizeof(className), __FILE__, __LINE__))
#define GAME_NEW_ARRAY(className, num) ((className *)AddTrack((unsigned int)new className[num], sizeof(className) * num, __FILE__, __LINE__))

#define GAME_DELETE(p) do { RemoveTrack((unsigned int) p); delete p; p = nullptr; } while (0)
#define GAME_DELETE_ARRAY(p) do { RemoveTrack((unsigned int) p); delete []p; p = nullptr; } while (0)

#define GAME_DUMP_LEAKS DumpUnfreed()

void *AddTrack(unsigned int addr, unsigned int size, const char *fileName, unsigned int lineNum);
void RemoveTrack(unsigned int addr);
void DumpUnfreed();

#else

#define GAME_NEW(className, params) new className params
#define GAME_NEW_ARRAY(className, num) new className[num]

#define GAME_DELETE(p) delete p
#define GAME_DELETE_ARRAY(p) delete []p

#define GAME_DUMP_LEAKS

#endif
