#include "stdafx.h"
#include "Globals.h"

#if ENABLE_LEAK_CONTROL > 0
#include <string>
#include <stdio.h>
#include <cstdlib>

struct ALLOC_INFO
{
    unsigned int    address;
    unsigned int    size;
    std::string     fileName;
    unsigned int    lineNum;
    ALLOC_INFO*     next;
};

ALLOC_INFO* gLeakList;

void *AddTrack(unsigned int addr, unsigned int size, const char* fileName, unsigned int lineNum)
{
    CUSTOM_ASSERT(addr, "Memory could not be allocated");
    ALLOC_INFO *info = new ALLOC_INFO();

    info->address  = addr;
    info->fileName = fileName;
    info->lineNum  = lineNum;
    info->size     = size;
    info->next     = gLeakList;
    gLeakList      = info;
    return (void *)addr;
};

void RemoveTrack(unsigned int addr)
{
    ALLOC_INFO *lastItem = 0;
    ALLOC_INFO *currItem = gLeakList;
    while (currItem) {
        if (currItem->address == addr) {
            if (lastItem)
                lastItem->next = currItem->next;
            else
                gLeakList = currItem->next;
            
            delete currItem;
            break;
        } else {
            lastItem = currItem;
            currItem = currItem->next;
        }
    }
};

#include <windows.h>

void DumpUnfreed()
{
    unsigned int totalSize = 0;
    char buf[1024];

    if (!gLeakList)
        return;

    ALLOC_INFO* currItem = gLeakList;

    OutputDebugStringA("******************* MEMORY LEAKS DETECTED ************************\n");

    while (currItem) {
        sprintf_s(buf, "%s(%d): ADDRESS %d\t%d unfreed\n", currItem->fileName.data(), currItem->lineNum, currItem->address, currItem->size);
        OutputDebugStringA(buf);
        totalSize += currItem->size;
        currItem = currItem->next;
    }

    sprintf_s(buf, "---------------------------------------------------------------------------------------\n");
    OutputDebugStringA(buf);
    sprintf_s(buf, "Total Memory Unfreed: %d bytes\n", totalSize);
    OutputDebugStringA(buf);
};

#endif
