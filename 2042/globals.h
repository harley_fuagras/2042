#pragma once

#define ENABLE_LEAK_CONTROL 1
#define ENABLE_GAME_ASSERT  1
#include "Debugging/LeaksDetector.h"
#include "Debugging/CustomAssert.h"
#include <cassert>

class World;
class Game;
class AppManager;
class TextureManager;
class LocalizationManager;
class GraphicEngine;
class EventManager;
class CollisionManager;
class SoundManager;

extern World*               gWorld;
extern Game*                gGame;
extern AppManager*          gAppManager;
extern TextureManager*      gTextureManager;
extern LocalizationManager* gLocalizationManager;
extern GraphicEngine*       gGraphicEngine;
extern EventManager*        gEventManager;
extern CollisionManager*    gCollisionManager;
extern SoundManager*        gSoundManager;

enum Direction {
    DIR_NONE,
    DIR_LEFT,
    DIR_RIGHT,
    DIR_UP,
    DIR_DOWN,
};

// GENERAL DATA
#define SCR_WIDTH      640
#define SCR_HEIGHT     480
#define SLEEP_TIME     17
#define GAMEOVER_TICKS 50
#define RESTART_TICKS  50

// INPUT DATA
#define KEY_MOVE_UP    SYS_KEY_UP
#define KEY_MOVE_LEFT  SYS_KEY_LEFT
#define KEY_MOVE_DOWN  SYS_KEY_DOWN
#define KEY_MOVE_RIGHT SYS_KEY_RIGHT
#define KEY_ACCEPT     VK_RETURN
#define KEY_SHOOT      VK_SPACE
#define KEY_PAUSE      VK_ESCAPE

// DATA DRIVEN PATHS
#define LEVELS_PATH  "data/def/levels.json"
#define ENEMIES_PATH "data/def/enemies.json"
#define WEAPONS_PATH "data/def/weapons.json"

// PLAYER DATA
#define PLAYER_INIT_LIFES       3
#define PLAYER_SIZE             vmake(32, 48)
#define PLAYER_INIT_POS         vmake(SCR_WIDTH / 2, SCR_HEIGHT - 450)
#define PLAYER_VELOCITY         vmake(5, 5)
#define PLAYER_END_VELOCITY     vmake(0, 10)
#define PLAYER_SHOOT_COOLDOWN   30 // Ticks.
#define PLAYER_ANIM_FPS         15 // Ticks.
#define PLAYER_SPRITES_PATHS    {\
                                    { "data/sprites/player/player_c_0.png", "data/sprites/player/player_c_1.png" },\
                                    { "data/sprites/player/player_l_0.png", "data/sprites/player/player_l_1.png" },\
                                    { "data/sprites/player/player_r_0.png", "data/sprites/player/player_r_1.png" },\
                                }
#define PLAYER_BULLET_VELOCITY  vmake(0, 5)
#define PLAYER_BULLET_SIZE      vmake(10, 26)
#define PLAYER_BULLET_TEX_PATH  {\
                                    { "data/sprites/bullets/laser_bullet/laser_bullet_0.png", "data/sprites/bullets/laser_bullet/laser_bullet_1.png" },\
                                }
#define PLAYER_HEALTH           1
#define PLAYER_COLLISION_DAMAGE 1000
#define PLAYER_BULLET_DAMAGE    100

// ENEMIES DATA
#define ENEMY_HRAND_COOLDOWN  45
#define ENEMY_ANIM_FPS        20 // Ticks.
#define ENEMY_BULLET_TEX_PATH {\
                                  { "data/sprites/bullets/ball_bullet/ball_bullet_0.png", "data/sprites/bullets/ball_bullet/ball_bullet_1.png" },\
                              }
#define ENEMY_BULLET_SIZE     vmake(10, 10)

// BULLETS DATA
#define BULLET_ANIM_FPS 10 // Ticks.

// EXPLOSIONS DATA
#define EXPLOSION_SIZE     vmake(32, 32)
#define EXPLOSION_ANIM_FPS 2
#define EXPLOSION_TEX_PATH {\
                               { "data/sprites/explosion/explosion_0.png",\
                                 "data/sprites/explosion/explosion_1.png",\
                                 "data/sprites/explosion/explosion_2.png",\
                                 "data/sprites/explosion/explosion_3.png",\
                                 "data/sprites/explosion/explosion_4.png",\
                               }\
                           }

// LOCALIZATION DATA
#define LOCALIZATION_BASE_PATH "data/localization/translations_"
#define LOCALES                { {"es", "CASTELLANO"}, {"en", "ENGLISH"} }

// MENUS DATA
#define MENU_BG_TEX_PATH      "data/backgrounds/bg_menu.png"
#define MENU_POS_Y_MAIN       250
#define MENU_POS_Y            400
#define MENU_TITLE_COLOR      rgbamake(2, 255, 2, 255)
#define MENU_TITLE_MARGIN     35
#define MENU_BTN_MARGIN       20
#define MENU_BTN_COLOR        rgbamake(255, 255, 255, 255)
#define MENU_BTN_COLOR_ACTIVE rgbamake(32, 144, 217, 255)
#define MENU_CHAR_WIDTH       16

// LEVEL TRANSITIONS DATA
#define L_TRANS_TEXT_COLOR rgbamake(255, 255, 255, 255)
#define L_TRANS_CHAR_WIDTH 16
#define L_TRANS_POS_Y      300
#define L_TRANS_MARGIN_Y   40
#define L_TRANS_BG_PATH    "data/backgrounds/bg_level_trans.png"
#define L_TRANS_TICKS      60

// HUD DATA
#define HUD_TEXT_COLOR   rgbamake(255, 255, 255, 255)
#define HUD_POS_Y        SCR_HEIGHT - 20
#define HUD_MARGIN_V     20
#define HUD_MARGIN_H     20
#define HUD_CHAR_WIDTH   16
#define SCORE_ZEROES_PAD 8

// SOUND DATA
#define MUSIC_MENU_PATH   "data/sound/menu_music.wav"
#define MUSIC_MENU_KEY    "menu_music"
#define MUSIC_GAME_PATH   "data/sound/game_music.wav"
#define MUSIC_GAME_KEY    "game_music"
#define FX_LASER_PATH     "data/sound/laser_fx.wav"
#define FX_LASER_KEY      "laser_fx"
#define FX_EXPLOSION_PATH "data/sound/explosion_fx.wav"
#define FX_EXPLOSION_KEY  "explosion_fx"
#define FX_SELECT_PATH    "data/sound/select_fx.wav"
#define FX_SELECT_KEY     "select_fx"
#define FX_HURT_PATH      "data/sound/hurt_fx.wav"
#define FX_HURT_KEY       "hurt_fx"
