#pragma once
#include <vector>
#include "lib/rapidjson/document.h"
#include "lib/rapidjson/filereadstream.h"
#include "lib/rapidjson/stringbuffer.h"
#include "GameUtils.h"
#include "EventManager.h"
#include "Menu.h"

class Background;
class Entity;
class Hud;

class World : public EventManager::Listener {
public:
    World();
    ~World();

    void Run();
    void AddEntity(Entity* entity);
    void AddEntityToDestroy(Entity* entity);
    void AddBackground(Background* background);
    void TogglePause(Menu::Type menu = Menu::Type::MS_NULL);
    bool CheckGameover();
    void GameOver();
    bool CheckChangeLevel();
    void ChangeLevel();
    bool CheckEnterLevelEndingMode();
    bool CheckRestartLevel();
    void EnterLevelEndingMode();
    void DisablePlayer();
    void DestroyEntity(Entity* entity);
    bool CheckEnemies();
    void RestartLevel();
    void ResetLevel();
    void RunEntities();
    void RemovePendingEntities();
    void AddPendingEntities();
    void RunCamera();
    void RunBackgrounds();
    void AddScore(int score);

    virtual bool ProcessEvent(const EventManager::CEvent& e);

    std::vector<Entity*> GetEntities() const;
    Entity*              GetPlayer() const;
    vec2                 GetCameraPos() const;
    vec2                 GetMapSize() const;
    bool                 GetIsPaused() const;
    bool                 GetIsGameOver() const;
    std::string          GetCurrentLevelName() const;
    std::string          GetNextLevelName() const;
    bool                 GetIsLevelEnding() const;
    bool                 GetWillChangeLevel() const;
    bool                 GetWillRestartLevel() const;
    Hud*                 GetHud() const;
    int                  GetScore() const;
    int                  GetPlayerLifes() const;

    void                 SetPlayer(Entity* player);
    void                 SetCameraVelocity(vec2 cameraVelocity);
    void                 SetCameraPos(vec2 cameraPos);
    void                 SetMapSize(vec2 mapSize);
    void                 SetIsPaused(bool isPaused);
    void                 SetIsGameOver(bool isGameover);
    void                 SetGameOverTicks(int gameOverTicks);
    void                 SetRestartLevelTicks(int restartLevelTicks);
    void                 SetCurrentLevelName(std::string& levelName);
    void                 SetNextLevelName(std::string& LevelName);
    void                 SetIsLevelEnding(bool isLevelEnding);
    void                 SetWillChangeLevel(bool willChangeLevel);
    void                 SetWillRestartLevel(bool willRestartLevel);
    void                 SetScore(int score);
    void                 SetPlayerLifes(int playerLifes);

private:
    Entity*                  mPlayer;
    std::vector<Entity*>     mEntities;
    std::vector<Entity*>     mEntitiesToAdd;
    std::vector<Entity*>     mEntitiesToDestroy;
    std::vector<Background*> mBackgrounds;
    std::string              mCurrentLevelName;
    std::string              mNextLevelName;
    vec2                     mMapSize;
    vec2                     mCameraPos;
    vec2                     mCameraVelocity;
    bool                     mIsPaused;
    bool                     mIsGameover;
    int                      mGameoverTicks;
    int                      mRestartLevelTicks;
    bool                     mIsLevelEnding;
    bool                     mWillChangeLevel;
    bool                     mWillRestartLevel;
    Hud*                     mHud;
    int                      mScore;
    int                      mPlayerLifes;

};
