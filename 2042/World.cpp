#include "stdafx.h"
#include "World.h"
#include "Globals.h"
#include "AppModeGame.h"
#include "Background.h"
#include "TextureManager.h"
#include "GraphicEngine.h"
#include "Menu.h"
#include "Hud.h"
#include "ComponentMessage.h"
#include "CollisionManager.h"

World::World() {
    mCameraPos         = vmake(0.0f, 0.0f);
    mGameoverTicks     = GAMEOVER_TICKS;
    mRestartLevelTicks = RESTART_TICKS;
    mIsLevelEnding     = false;
    mWillChangeLevel   = false;
    mWillRestartLevel  = false;
    mScore             = 0;
    mPlayerLifes       = PLAYER_INIT_LIFES;
    mHud               = GAME_NEW(Hud, (mScore, mPlayerLifes));
    mPlayer            = NewPlayer();
    AddEntity(mPlayer);
    gEventManager->Register(this);
}

World::~World() {
    for (auto& entity : mEntities) {
        GAME_DELETE(entity);
        entity = nullptr;
    }
    gEventManager->Unregister(this);
    
    GAME_DELETE(mHud);
    mHud = nullptr;
}

void World::Run() {
    if (!mIsPaused) {
        RunCamera();
        RemovePendingEntities();
        AddPendingEntities();
        RunEntities();
        RunBackgrounds();
        
        if (CheckRestartLevel())
            RestartLevel();
        if(CheckGameover())
            GameOver();
        if(CheckChangeLevel())
            ChangeLevel();
        if (CheckEnterLevelEndingMode())
            EnterLevelEndingMode();
    }
}

void World::AddEntity(Entity* entity) {
    mEntitiesToAdd.push_back(entity);
}

void World::AddEntityToDestroy(Entity* entity) {
    mEntitiesToDestroy.push_back(entity);
}

void World::AddBackground(Background* background) {
    mBackgrounds.push_back(background);
    gGraphicEngine->AddBackground(background);
}

void World::TogglePause(Menu::Type menu) {
    GetPlayerControllerCMsg getPlayerControllerMessage;
    mPlayer->ReceiveMessage(&getPlayerControllerMessage);

    if (GetIsPaused()) {
        gGraphicEngine->SetWillClearMenuState(true);
        // Register player controller to event manager.
        gEventManager->Register(getPlayerControllerMessage.playerController);
        SetIsPaused(false);
    } else {
        // Create pause menu.
        Menu* menuState = GAME_NEW(Menu, (menu));
        // Unregister player controller to event manager.
        gEventManager->Unregister(getPlayerControllerMessage.playerController);
        SetIsPaused(true);
    }
}

bool World::CheckGameover() {
    if (GetIsGameOver() && mGameoverTicks > 0) {
        mGameoverTicks--;
        if (mGameoverTicks <= 0) 
            return true;
    }
    return false;
}

void World::GameOver() {
    Menu* menuState = GAME_NEW(Menu, (Menu::MS_GAMEOVER));
    SetIsPaused(true);
}

bool World::CheckChangeLevel() {
    return mWillChangeLevel && !gWorld->GetIsGameOver();
}

void World::ChangeLevel() {
    // Check if there is a next level. If there is not, go to Main Menu.
    if (gAppManager->GetWantedOptionId() != "") {
        // Reenable player control.
        SetIsActiveControllerCMsg setIsActiveControllerCMsg{ true };
        mPlayer->ReceiveMessage(&setIsActiveControllerCMsg);

        GetPlayerControllerCMsg getPlayerControllerMessage;
        mPlayer->ReceiveMessage(&getPlayerControllerMessage);
        gEventManager->Register(getPlayerControllerMessage.playerController);

        // Reset player velocity.
        SetVelocityCMsg setVelocityMsg{ PLAYER_VELOCITY };
        mPlayer->ReceiveMessage(&setVelocityMsg);

        gAppManager->SetWantedModeId(AppManager::AM_LEVEL_TRANSITION);
    } else {
        SetWillChangeLevel(false);
        gAppManager->SetWantedModeId(AppManager::AM_MENU);
    }
}

bool World::CheckEnterLevelEndingMode() {
    // Check if we have arrived to the end of level and there are no enemies at level.
    return ((mCameraPos.y > mMapSize.y - SCR_HEIGHT) && !CheckEnemies() && !mIsLevelEnding && !mIsGameover);
}

bool World::CheckRestartLevel() {

    if (GetWillRestartLevel() && mRestartLevelTicks > 0) {
        mRestartLevelTicks--;
        if (mRestartLevelTicks <= 0)
            return true;
    }
    return false;
}

void World::EnterLevelEndingMode() {
    mIsLevelEnding = true;

    // Stop camera movement.
    mCameraVelocity = vmake(0.0f, 0.0f);
    
    DisablePlayer();

    // Set player auto velocity.
    SetVelocityCMsg setVelocityMsg{ PLAYER_END_VELOCITY };
    mPlayer->ReceiveMessage(&setVelocityMsg);
}

void World::DisablePlayer() {
    // Disable player control.
    SetIsActiveControllerCMsg setIsActiveControllerCMsg{ false };
    mPlayer->ReceiveMessage(&setIsActiveControllerCMsg);

    GetPlayerControllerCMsg getPlayerControllerMessage;
    mPlayer->ReceiveMessage(&getPlayerControllerMessage);
    gEventManager->Unregister(getPlayerControllerMessage.playerController);
}

void World::DestroyEntity(Entity* entity) {
    GetDoesExplodeCMsg getDoesExplodeMessage;
    entity->ReceiveMessage(&getDoesExplodeMessage);
    if (getDoesExplodeMessage.doesExplode) {
        SpawnExplosionCMsg spawnExplosionMsg;
        entity->ReceiveMessage(&spawnExplosionMsg);
    }

    if (entity->GetType() == Entity::Type::E_PLAYER) {
        mPlayerLifes--;
        mHud->UpdatePlayerLifesText(mPlayerLifes);

        if (mPlayerLifes <= 0) {
            SetIsGameOver(true);
            GAME_DELETE(entity);
            entity = nullptr;
        } else {
            // Hide player.
            GetSpriteCMsg getSpriteMsg;
            mPlayer->ReceiveMessage(&getSpriteMsg);
            getSpriteMsg.sprite->SetIsVisible(false);

            // Reset player health.
            SetCurrentHealthCMsg setCurrentHealthMsg = { PLAYER_HEALTH };
            mPlayer->ReceiveMessage(&setCurrentHealthMsg);

            SetWillRestartLevel(true);
        }
        
    } else {
        GAME_DELETE(entity);
        entity = nullptr;
    }
}

bool World::CheckEnemies() {
    for (auto entity = mEntities.cbegin(); entity != mEntities.cend(); entity++) {
        GetFactionCMsg getFactionMsg;
        (*entity)->ReceiveMessage(&getFactionMsg);
        if (getFactionMsg.typeFaction == C_Faction::F_ENEMY) {
            (*entity)->GetType();
            return true;
        }
    }
    return false;
}

void World::RestartLevel() {

    AddEntity(GetPlayer());

    //Restart current level.
    gAppManager->SetWantedOptionId(mCurrentLevelName);
    gAppManager->SetWantedModeId(AppManager::AM_LEVEL_TRANSITION);
}

void World::ResetLevel() {
    SetCameraPos(vmake(0.0f, 0.0f));
    SetGameOverTicks(GAMEOVER_TICKS);
    SetRestartLevelTicks(RESTART_TICKS);
    SetIsLevelEnding(false);
    SetWillChangeLevel(false);
    SetWillRestartLevel(false);

    // Show player again.
    GetSpriteCMsg getSpriteMsg;
    mPlayer->ReceiveMessage(&getSpriteMsg);
    getSpriteMsg.sprite->SetIsVisible(true);

    // Reset player position.
    mPlayer->SetPos(PLAYER_INIT_POS);
}

void World::RunEntities() {
    for (auto entity = mEntities.cbegin(); entity != mEntities.cend(); ++entity)
        (*entity)->Run();
}

void World::RemovePendingEntities() {
    for (auto entityToRemove = mEntitiesToDestroy.cbegin(); entityToRemove != mEntitiesToDestroy.cend(); ++entityToRemove) {
        auto it2 = mEntities.begin();
        bool deleted = false;
        while (!deleted && (it2 != mEntities.end())) {
            if (*entityToRemove == *it2) {
                DestroyEntity(*it2);
                mEntities.erase(it2);
                deleted = true;
            } else
                ++it2;
        }
    }
    mEntitiesToDestroy.clear();
}

void World::AddPendingEntities() {
    for (auto entityToAdd = mEntitiesToAdd.cbegin(); entityToAdd != mEntitiesToAdd.cend(); ) {
        mEntities.push_back(*entityToAdd);
        entityToAdd = mEntitiesToAdd.erase(entityToAdd);
    }
}

void World::RunCamera() {
    mCameraPos = vadd(mCameraPos, mCameraVelocity);
}

void World::RunBackgrounds() {
    for (auto background = mBackgrounds.cbegin(); background != mBackgrounds.cend(); ++background)
        (*background)->Run();
}

void World::AddScore(int score) {
    mScore += score;
    if (mScore > pow(10, SCORE_ZEROES_PAD) - 1)
        mScore = 0;
    mHud->UpdateScoreText(mScore);
}

std::vector<Entity*> World::GetEntities() const {
    return mEntities;
}

Entity* World::GetPlayer() const {
    return mPlayer;
}

vec2 World::GetCameraPos() const {
    return mCameraPos;
}

vec2 World::GetMapSize() const {
    return mMapSize;
}

bool World::GetIsPaused() const {
    return mIsPaused;
}

bool World::GetIsGameOver() const {
    return mIsGameover;
}

std::string World::GetCurrentLevelName() const {
    return mCurrentLevelName;
}

std::string World::GetNextLevelName() const {
    return mNextLevelName;
}

bool World::GetIsLevelEnding() const {
    return mIsLevelEnding;
}

bool World::GetWillChangeLevel() const {
    return mWillChangeLevel;
}

bool World::GetWillRestartLevel() const {
    return mWillRestartLevel;
}

Hud* World::GetHud() const {
    return mHud;
}

int World::GetScore() const {
    return mScore;
}

int World::GetPlayerLifes() const {
    return mPlayerLifes;
}

void World::SetPlayer(Entity* player) {
    mPlayer = player;
}

void World::SetCameraVelocity(vec2 cameraVelocity) {
    mCameraVelocity = cameraVelocity;
}

void World::SetCameraPos(vec2 cameraPos) {
    mCameraPos = cameraPos;
}

void World::SetMapSize(vec2 mapSize) {
    mMapSize = mapSize;
}

void World::SetIsPaused(bool isPaused) {
    mIsPaused = isPaused;
}

void World::SetIsGameOver(bool isGameover) {
    mIsGameover = isGameover;
}

void World::SetGameOverTicks(int gameOverTicks) {
    mGameoverTicks = gameOverTicks;
}

void World::SetRestartLevelTicks(int restartLevelTicks) {
    mRestartLevelTicks = restartLevelTicks;
}

void World::SetCurrentLevelName(std::string& levelName) {
    mCurrentLevelName = levelName;
}

void World::SetNextLevelName(std::string& levelName) {
    mNextLevelName = levelName;
}

void World::SetIsLevelEnding(bool isLevelEnding) {
    mIsLevelEnding = isLevelEnding;
}

void World::SetWillChangeLevel(bool willChangeLevel) {
    mWillChangeLevel = willChangeLevel;
}

void World::SetWillRestartLevel(bool willRestartLevel) {
    mWillRestartLevel = willRestartLevel;
}

bool World::ProcessEvent(const EventManager::CEvent& e) {
    EventManager::TEvent eventType = e.GetType();

    switch (eventType) {
        case EventManager::TEvent::E_KEY_ESCAPE_SINGLE: {
            TogglePause(Menu::MS_PAUSE);
            return true;
        }
    }
    return false;
}

void World::SetScore(int score) {
    mScore = score;
}

void World::SetPlayerLifes(int playerLifes) {
    mPlayerLifes = playerLifes;
}
