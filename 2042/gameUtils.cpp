#include "stdafx.h"
#include "lib/sys.h"
#include "GameUtils.h"
#include "Globals.h"
#include "Game.h"
#include "Entity.h"
#include "SoundManager.h"
#include "CollisionManager.h"
#include "C_Destructible.h"
#include "C_Weapon.h"
#include "C_OutScreenSpawn.h"
#include "C_Faction.h"
#include "C_PlayerController.h"
#include "C_SimpleMovement.h"
#include "C_Collider.h"
#include "IC_AI.h"
#include "C_AI_ZigZag.h"
#include "C_AI_HRand.h"
#include "C_Render.h"
#include "C_Points.h"
#include "C_StateMachine.h"
#include <map>

std::random_device rd;
std::mt19937 gMersenne(rd());

IC_AI* GetAIComponent(std::string& aiId, Entity* entity) {
    auto it = AITypes.find(aiId);
    IC_AI* c_ai = nullptr;
    
    if (it != AITypes.end()) {
        IC_AI::Type AIType = AITypes.find(aiId)->second;
        switch (AIType) {
            case IC_AI::AI_ZIGZAG: {
                c_ai = GAME_NEW(C_AI_ZigZag, (entity));
                break;
            }
            case IC_AI::AI_HRAND: {
                c_ai = GAME_NEW(C_AI_HRand, (entity));
                break;
            }
            default: {
                break;
            }
        }
    }
    return c_ai;
}

Entity* NewPlayer() {
    Entity* entity = GAME_NEW(Entity, (PLAYER_INIT_POS, PLAYER_SIZE, Entity::Type::E_PLAYER));
    CUSTOM_ASSERT(entity, "Player entity is null");

    std::vector<std::vector<std::string>> texPaths = PLAYER_SPRITES_PATHS;
    C_Render* cRender                              = GAME_NEW(C_Render, (texPaths, PLAYER_ANIM_FPS, true, false, entity));
    C_Faction *cFaction                            = GAME_NEW(C_Faction, (C_Faction::Type::F_PLAYER, entity));
    C_Collider *cCollider                          = GAME_NEW(C_Collider, (entity, PLAYER_COLLISION_DAMAGE));
    C_PlayerController *cPlayerController          = GAME_NEW(C_PlayerController, (PLAYER_VELOCITY, PLAYER_SHOOT_COOLDOWN, entity));
    C_Destructible* cDestructible                  = GAME_NEW(C_Destructible, (true, PLAYER_HEALTH, entity));

    entity->AddComponent(cRender);
    entity->AddComponent(cFaction);
    entity->AddComponent(cCollider);
    entity->AddComponent(cPlayerController);
    entity->AddComponent(cDestructible);

    gCollisionManager->AddCollider(cCollider);

    return entity;
}

Entity* NewEnemy(
    vec2 pos, vec2 spawnPos, vec2 size, vec2 velocity, std::vector<std::vector<std::string>>& texPath,
    std::string& aiId, std::string& stateMachinePath, std::vector<std::string>& weaponIds,
    int points, float health, float collisionDamage
) {
    Entity* entity = GAME_NEW(Entity, (pos, size, Entity::Type::E_ENEMY));
    CUSTOM_ASSERT(entity, "Enemy entity is null");

    C_Render* cRender = GAME_NEW(C_Render, (texPath, ENEMY_ANIM_FPS, true, false, entity));
    entity->AddComponent(cRender);

    C_Faction* cFaction = GAME_NEW(C_Faction, (C_Faction::Type::F_ENEMY, entity));
    entity->AddComponent(cFaction);
    
    C_SimpleMovement* cSimpleMovement = GAME_NEW(C_SimpleMovement, (velocity, false, entity));
    entity->AddComponent(cSimpleMovement);
    
    C_OutscreenSpawn* cOutscreenSpawn = GAME_NEW(C_OutscreenSpawn, (spawnPos, entity));
    entity->AddComponent(cOutscreenSpawn);
    
    C_Destructible* cDestructible = GAME_NEW(C_Destructible, (true, health, entity));
    entity->AddComponent(cDestructible);
    
    C_Collider* cCollider = GAME_NEW(C_Collider, (entity, collisionDamage));
    entity->AddComponent(cCollider);
    
    C_Points* cPoints = GAME_NEW(C_Points, (points, entity));
    entity->AddComponent(cPoints);
    
    IC_AI* cAi = GetAIComponent(aiId, entity);
    if(cAi)
        entity->AddComponent(cAi);

    for (auto& weaponId : weaponIds) {
        C_Weapon* cWeapon = C_Weapon::Load(weaponId, entity);
        entity->AddComponent(cWeapon);
    }

    if (stateMachinePath != "") {
        C_StateMachine* cStateMachine = GAME_NEW(C_StateMachine, (entity, stateMachinePath));
        entity->AddComponent(cStateMachine);
        cStateMachine->Start();
    }

    return entity;
}

Entity* NewBullet(vec2 pos, vec2 velocity, float damage, C_Faction::Type typeFaction) {

    std::vector <std::vector <std::string>> bulletTex;
    vec2                                    bulletSize;
    switch (typeFaction) {
        case C_Faction::F_PLAYER: {
            bulletTex  = PLAYER_BULLET_TEX_PATH;
            bulletSize = PLAYER_BULLET_SIZE;
            break;
        }
        case C_Faction::F_ENEMY: {
            bulletTex  = ENEMY_BULLET_TEX_PATH;
            bulletSize = ENEMY_BULLET_SIZE;
            break;
        }
    }

    Entity* entity = GAME_NEW(Entity, (pos, bulletSize, Entity::Type::E_BULLET));
    CUSTOM_ASSERT(entity, "Bullet entity is null");

    C_Render* cRender                 = GAME_NEW(C_Render, (bulletTex, BULLET_ANIM_FPS, true, false, entity));
    C_Faction* cFaction               = GAME_NEW(C_Faction, (typeFaction, entity));
    C_Collider* cCollider             = GAME_NEW(C_Collider, (entity, damage));
    C_SimpleMovement* cSimpleMovement = GAME_NEW(C_SimpleMovement, (velocity, true, entity));
    C_Destructible* cDestructible     = GAME_NEW(C_Destructible, (false, 0.0f, entity));

    entity->AddComponent(cRender);
    entity->AddComponent(cFaction);
    entity->AddComponent(cCollider);
    entity->AddComponent(cSimpleMovement);
    entity->AddComponent(cDestructible);

    gCollisionManager->AddCollider(cCollider);

    return entity;
}

Entity* NewExplosion(vec2 pos) {
    Entity*  entity = GAME_NEW(Entity, (pos, EXPLOSION_SIZE, Entity::Type::E_EXPLOSION));
    CUSTOM_ASSERT(entity, "New explosion entity is null");

    std::vector<std::vector<std::string>> texPaths = EXPLOSION_TEX_PATH;
    
    C_Render* c_render             = GAME_NEW(C_Render, (texPaths, EXPLOSION_ANIM_FPS, false, true, entity));
    C_Destructible* c_destructible = GAME_NEW(C_Destructible, (false, 0.0f, entity));

    entity->AddComponent(c_render);
    entity->AddComponent(c_destructible);
    
    std::string explosionKey = FX_EXPLOSION_KEY;
    gSoundManager->PlaySound(explosionKey);

    return entity;
}

bool CheckRectCollision(vec2 r1Pos, vec2 r1Size, vec2 r2Pos, vec2 r2Size) {
    return (max(r1Pos.x, r2Pos.x) < min(r1Pos.x + r1Size.x, r2Pos.x + r2Size.x) &&
        min(r1Pos.y, r2Pos.y) > max(r1Pos.y - r1Size.y, r2Pos.y - r2Size.y));
}

int GetRandInt(int min, int max) {
    std::uniform_int_distribution<int> getRand{ min, max };
    return getRand(gMersenne);
}

float GetLineHCenterPos(float width, int textLength, float charWidth) {
    return width / 2.0f - static_cast<float>(textLength) * charWidth / 2.0f;
}
