#pragma once
#include "lib/core.h"
#include <vector>

class  Component;
struct ComponentMessage;

class Entity {
public:
    enum Type {
        E_PLAYER,
        E_ENEMY,
        E_BULLET,
        E_EXPLOSION,
    };

    Entity(vec2 pos, vec2 size, Type type);
    ~Entity();

    virtual void Run();
    void         AddComponent(Component* component);
    void         ReceiveMessage(ComponentMessage* msg);
    
    void SetPos(vec2 pos);
    void SetSize(vec2 size);
    vec2 GetPos() const;
    vec2 GetSize() const;
    Type GetType() const;

private:
    std::vector<Component*> mComponentsToAdd;
    std::vector<Component*> mComponents;
    Type                    mType;
    vec2                    mPos;
    vec2                    mSize;

};
