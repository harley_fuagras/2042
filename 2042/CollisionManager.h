#pragma once
#include <vector>
#include "C_Faction.h"

class C_Collider;

class CollisionManager {
public:
    void Run();
    void Clear();
    C_Faction::Type GetFaction(C_Collider* collider);
    void AddCollider(C_Collider* collider);
    void RemoveCollider(C_Collider* collider);

private:
    std::vector<C_Collider*> mPlayerColliders;
    std::vector<C_Collider*> mEnemyColliders;
};
