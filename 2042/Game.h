#pragma once
#include "lib/sys.h"
#include "lib/core.h"
#include "lib/font.h"
#include "lib/rapidjson/document.h"
#include "lib/rapidjson/filereadstream.h"
#include "lib/rapidjson/stringbuffer.h"
#include <vector>
#include "GameUtils.h"

class Game {
public:
    struct EnemyLevelData {
        std::string id;
        vec2        initPos;
        vec2        spawnPos;
    };

    struct BgLevelData {
        vec2        size;
        vec2        pos;
        vec2        scrollRatio;
        std::string texPath;
    };

    struct LevelData {
        std::string                 id;
        vec2                        size;
        vec2                        cameraVelocity;
        std::string                 nextLevel;
        std::vector<EnemyLevelData> enemyLevelData;
        std::vector<BgLevelData>    bgLevelData;
        std::string                 levelName;
    };

    struct EnemyData {
        std::string                           id;
        std::vector<std::vector<std::string>> texPaths;
        vec2                                  velocity;
        vec2                                  size;
        std::string                           aiId;
        std::string                           stateMachinePath;
        std::vector<std::string>              weaponIds;
        int                                   points;
        float                                 health;
        float                                 collisionDamage;
    };

    struct WeaponData {
        virtual ~WeaponData() {}
        std::string id;
        std::string type;
        int         cooldown;
        int         chance;
        float       damage;
        float       bulletSpeed;
    };

    struct WeaponRadialData : public WeaponData {
        float initialAngle;
        float angleVariation; // Initial angle modification after every shoot.
        int   bulletsNumber;
    };

    struct WeaponMultipleData : public WeaponData {
        float angleSeparation;
        int   bulletsNumber;
    };

    Game();
    ~Game();

    void LoadLevelsData();
    void LoadEnemiesData();
    void LoadWeaponsData();
    
    bool                               GetQuit() const;
    bool                               GetIsMusicOn() const;
    bool                               GetIsSoundFxOn() const;
    std::map<std::string, LevelData>   GetLevelsData() const;
    std::map<std::string, EnemyData>   GetEnemiesData() const;
    std::map<std::string, WeaponData*> GetWeaponsData() const;
    void                               SetQuit(bool quit);
    void                               SetIsMusicOn(bool isMusicOn);
    void                               SetIsSoundFXOn(bool isSoundFXOn);
    void                               ToggleMusic();
    void                               ToggleSoundFX();

private:
    bool                               mQuit;
    bool                               mIsMusicOn;
    bool                               mIsSoundFXOn;
    std::map<std::string, LevelData>   mLevelsData;
    std::map<std::string, EnemyData>   mEnemiesData;
    std::map<std::string, WeaponData*> mWeaponsData;

};
