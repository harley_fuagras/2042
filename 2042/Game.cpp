#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "TextureManager.h"

Game::Game()
    : mQuit(false), mIsMusicOn(true), mIsSoundFXOn(true)
{
    LoadLevelsData();
    LoadEnemiesData();
    LoadWeaponsData();
}

Game::~Game() {
    for (auto& weaponData : mWeaponsData) {
        GAME_DELETE(weaponData.second);
        weaponData.second = nullptr;
    }
}

void Game::LoadLevelsData() {
    // Read levels data from json.
    FILE* levelsDoc;
    fopen_s(&levelsDoc, LEVELS_PATH, "rb");

    CUSTOM_ASSERT_FILE(levelsDoc, "Levels data file", LEVELS_PATH);
    char levelsBuffer[65536];
    rapidjson::FileReadStream levelsStream(levelsDoc, levelsBuffer, sizeof(levelsBuffer));
    rapidjson::Document levels;
    levels.ParseStream(levelsStream);
    std::vector<std::vector<std::string>> locales = LOCALES;

    for (auto& level : levels.GetObject()) {
        LevelData levelData;
        levelData.id             = level.name.GetString();
        levelData.size           = vmake(level.value["sizeX"].GetFloat(), level.value["sizeY"].GetFloat());
        levelData.cameraVelocity = vmake(level.value["cameraVelocityX"].GetFloat(), level.value["cameraVelocityY"].GetFloat());
        levelData.nextLevel      = level.value["nextLevel"].GetString();
        levelData.levelName      = level.value["name"].GetString();

        const rapidjson::Value& backgrounds = level.value["backgrounds"];
        CUSTOM_ASSERT(backgrounds.IsArray(), "backgrounds is not an array. Most likely there is a format error on levels.json file");
        for (rapidjson::Value::ConstValueIterator itr = backgrounds.Begin(); itr != backgrounds.End(); ++itr) {
            BgLevelData bgLevelData;
            bgLevelData.size        = vmake((*itr)["sizeX"].GetFloat(), (*itr)["sizeY"].GetFloat());
            bgLevelData.pos         = vmake((*itr)["posX"].GetFloat(), (*itr)["posY"].GetFloat());
            bgLevelData.scrollRatio = vmake((*itr)["scrollRatioX"].GetFloat(), (*itr)["scrollRatioY"].GetFloat());
            bgLevelData.texPath     = (*itr)["texPath"].GetString();
            levelData.bgLevelData.push_back(bgLevelData);
        }

        const rapidjson::Value& enemies = level.value["enemies"];
        CUSTOM_ASSERT(enemies.IsArray(), "enemies is not an array. Most likely there is a format error on levels.json file");
        for (rapidjson::Value::ConstValueIterator itr = enemies.Begin(); itr != enemies.End(); ++itr) {
            EnemyLevelData enemyLevelData;
            enemyLevelData.id       = (*itr)["id"].GetString();
            enemyLevelData.initPos  = vmake((*itr)["initPosX"].GetFloat(), (*itr)["initPosY"].GetFloat());
            enemyLevelData.spawnPos = vmake((*itr)["spawnPosX"].GetFloat(), (*itr)["spawnPosY"].GetFloat());
            levelData.enemyLevelData.push_back(enemyLevelData);
        }
        mLevelsData.insert(std::make_pair(levelData.id, levelData));
    }
    fclose(levelsDoc);
}

void Game::LoadEnemiesData() {
    // Read enemies data from json.
    FILE* enemiesDoc;
    fopen_s(&enemiesDoc, ENEMIES_PATH, "rb");

    char enemiesBuffer[65536];
    rapidjson::FileReadStream enemiesStream(enemiesDoc, enemiesBuffer, sizeof(enemiesBuffer));
    rapidjson::Document enemies;
    enemies.ParseStream(enemiesStream);

    for (auto& enemy : enemies.GetObject()) {
        EnemyData enemyData;
        enemyData.id               = enemy.name.GetString();
        enemyData.velocity         = vmake(enemy.value["velocityX"].GetFloat(), enemy.value["velocityY"].GetFloat());
        enemyData.size             = vmake(enemy.value["sizeX"].GetFloat(), enemy.value["sizeY"].GetFloat());
        enemyData.aiId             = enemy.value["aiId"].GetString();
        enemyData.stateMachinePath = enemy.value["stateMachinePath"].GetString();
        enemyData.points           = enemy.value["points"].GetInt();
        enemyData.health           = enemy.value["health"].GetFloat();
        enemyData.collisionDamage  = enemy.value["collisionDamage"].GetFloat();

        const rapidjson::Value& texPathsSrc = enemy.value["texPaths"];
        CUSTOM_ASSERT(texPathsSrc.IsArray(), "texPathsSrc is not an array. Most likely there is a format error on enemies.json file");
        enemyData.texPaths.push_back(std::vector<std::string>());
        for (rapidjson::Value::ConstValueIterator itr = texPathsSrc.Begin(); itr != texPathsSrc.End(); ++itr) {
            std::string path = (*itr)["path"].GetString();
            enemyData.texPaths[0].push_back(path);
        }

        const rapidjson::Value& weaponsSrc = enemy.value["weapons"];
        CUSTOM_ASSERT(weaponsSrc.IsArray(), "weaponsSrc is not an array. Most likely there is a format error on enemies.json file");
        for (rapidjson::Value::ConstValueIterator itr = weaponsSrc.Begin(); itr != weaponsSrc.End(); ++itr) {
            std::string weaponId = (*itr)["weaponId"].GetString();
            enemyData.weaponIds.push_back(weaponId);
        }

        mEnemiesData.insert(std::make_pair(enemyData.id, enemyData));
    }
    fclose(enemiesDoc);
}

void Game::LoadWeaponsData() {
    // Read weapons data from json.
    FILE* weaponsDoc;
    fopen_s(&weaponsDoc, WEAPONS_PATH, "rb");

    char weaponsBuffer[65536];
    rapidjson::FileReadStream weaponsStream(weaponsDoc, weaponsBuffer, sizeof(weaponsBuffer));
    rapidjson::Document weapons;
    weapons.ParseStream(weaponsStream);

    for (auto& weapon : weapons.GetObject()) {
        WeaponData* weaponData = nullptr;
        std::string weaponId   = weapon.name.GetString();
        std::string weaponType = weapon.value["type"].GetString();

        if (weaponType == "radial") {
            WeaponRadialData* weaponRadialData = GAME_NEW(WeaponRadialData, ());
            weaponRadialData->initialAngle     = weapon.value["initialAngle"].GetFloat();
            weaponRadialData->angleVariation   = weapon.value["angleVariation"].GetFloat();
            weaponRadialData->bulletsNumber    = weapon.value["bulletsNumber"].GetInt();
            weaponData = weaponRadialData;
        } else if (weaponType == "multiple") {
            WeaponMultipleData* weaponMultipleData = GAME_NEW(WeaponMultipleData, ());
            weaponMultipleData->angleSeparation    = weapon.value["angleSeparation"].GetFloat();
            weaponMultipleData->bulletsNumber      = weapon.value["bulletsNumber"].GetInt();
            weaponData = weaponMultipleData;
        } else {
            weaponData = GAME_NEW(WeaponData, ());
        }

        weaponData->id          = weaponId;
        weaponData->type        = weaponType;
        weaponData->cooldown    = weapon.value["cooldown"].GetInt();
        weaponData->chance      = weapon.value["chance"].GetInt();
        weaponData->damage      = weapon.value["damage"].GetFloat();
        weaponData->bulletSpeed = weapon.value["bulletSpeed"].GetFloat();

        mWeaponsData.insert(std::make_pair(weaponId, weaponData));
    }

    fclose(weaponsDoc);
}

bool Game::GetQuit() const {
    return mQuit;
}

std::map<std::string, Game::LevelData> Game::GetLevelsData() const {
    return mLevelsData;
}

std::map<std::string, Game::EnemyData> Game::GetEnemiesData() const {
    return mEnemiesData;
}

std::map<std::string, Game::WeaponData*> Game::GetWeaponsData() const {
    return mWeaponsData;
}

bool Game::GetIsMusicOn() const {
    return mIsMusicOn;
}

bool Game::GetIsSoundFxOn() const {
    return mIsSoundFXOn;
}

void Game::SetQuit(bool quit) {
    mQuit = quit;
}

void Game::SetIsMusicOn(bool isMusicOn) {
    mIsMusicOn = isMusicOn;
}

void Game::SetIsSoundFXOn(bool isSoundFXOn) {
    mIsSoundFXOn = isSoundFXOn;
}

void Game::ToggleMusic() {
    if (GetIsMusicOn())
        SetIsMusicOn(false);
    else
        SetIsMusicOn(true);
}

void Game::ToggleSoundFX() {
    if (GetIsSoundFxOn())
        SetIsSoundFXOn(false);
    else
        SetIsSoundFXOn(true);
}
