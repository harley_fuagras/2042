#pragma once
#include "Entity.h"
#include "ComponentMessage.h"

class C_Collider : public Component {
private:
    float mDamage;

public:
    C_Collider(Entity* owner, float damage);
    
    Entity* GetOwner() const;
    vec2    GetPos() const;
    vec2    GetSize() const;
    float   GetDamage() const;

    void ReceiveMessage(ComponentMessage* msg);

};
