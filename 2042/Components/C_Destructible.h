#pragma once
#include "Component.h"

class Entity;

class C_Destructible : public Component {
public:
    C_Destructible(bool doesExplode, float mHealth, Entity* owner);
    void  Run();
    void  SetWillBeDestroyed(bool willBeDestroyed, bool checkPoints);
    void  SetDoesExplode(bool doesExplode);
    void  SetCurrentHealth(float health);
    bool  GetWillBeDestroyed() const;
    bool  GetDoesExplode() const;
    float GetCurrentHealthRatio() const;
    void  Hurt(float Damage);
    void  ReceiveMessage(ComponentMessage* msg);

private:
    float mHealth;
    float mCurrentHealth;
    bool  mWillBeDestroyed;
    bool  mDoesExplode;

};
