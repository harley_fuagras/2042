#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "World.h"
#include "C_Destructible.h"
#include "ComponentMessage.h"
#include "CollisionManager.h"
#include "GraphicEngine.h"
#include "Hud.h"
#include "AppManager.h"
#include "SoundManager.h"

C_Destructible::C_Destructible(bool doesExplode, float health, Entity* owner)
    : mWillBeDestroyed(false), mHealth(health), mCurrentHealth(health), mDoesExplode(doesExplode)
{
    mOwner = owner;
}

void C_Destructible::Run() {
}

void C_Destructible::SetWillBeDestroyed(bool willBeDestroyed, bool checkPoints) {
    mWillBeDestroyed = willBeDestroyed;
    if (willBeDestroyed) {
        gWorld->AddEntityToDestroy(mOwner);
        if (checkPoints && mOwner->GetType() == Entity::Type::E_ENEMY) {
            GetPointsCMsg getPointsCMsg;
            mOwner->ReceiveMessage(&getPointsCMsg);
            gWorld->AddScore(getPointsCMsg.points);
        }
    }
}

void C_Destructible::SetDoesExplode(bool doesExplode) {
    mDoesExplode = doesExplode;
}

void C_Destructible::SetCurrentHealth(float health) {
    mCurrentHealth = health;
}

bool C_Destructible::GetWillBeDestroyed() const {
    return mWillBeDestroyed;
}

bool C_Destructible::GetDoesExplode() const {
    return mDoesExplode;
}

float C_Destructible::GetCurrentHealthRatio() const {
    return mCurrentHealth / mHealth;
}

void C_Destructible::Hurt(float damage) {
    mCurrentHealth -= damage;
    if (mCurrentHealth <= 0) {
        SetWillBeDestroyed(true, true);
    } else {
        std::string hurtKey = FX_HURT_KEY;
        gSoundManager->PlaySound(hurtKey);
    }
}

void C_Destructible::ReceiveMessage(ComponentMessage* msg) {
    HurtCMsg *hurtMsg = dynamic_cast<HurtCMsg*>(msg);
    if (hurtMsg) {
        Hurt(hurtMsg->damage);
        return;
    }

    SetCurrentHealthCMsg* setCurrentHealthMsg = dynamic_cast<SetCurrentHealthCMsg*>(msg);
    if (setCurrentHealthMsg) {
        SetCurrentHealth(setCurrentHealthMsg->health);
        return;
    }

    GetCurrentHealthRatioCMsg* getCurrentHealthRatioMsg = dynamic_cast<GetCurrentHealthRatioCMsg*>(msg);
    if (getCurrentHealthRatioMsg) {
        getCurrentHealthRatioMsg->ratio = GetCurrentHealthRatio();
        return;
    }

    SetWillBeDestroyedCMsg* setWillBeDestroyedMsg = dynamic_cast<SetWillBeDestroyedCMsg*>(msg);
    if (setWillBeDestroyedMsg) {
        SetWillBeDestroyed(setWillBeDestroyedMsg->willBeDestroyed, setWillBeDestroyedMsg->checkPoints);
        return;
    }

    GetWillBeDestroyedCMsg* getWillBeDetroyedMsg = dynamic_cast<GetWillBeDestroyedCMsg*>(msg);
    if (getWillBeDetroyedMsg) {
        getWillBeDetroyedMsg->willBeDestroyed = GetWillBeDestroyed();
        return;
    }

    GetDoesExplodeCMsg* getDoesExplodeMsg = dynamic_cast<GetDoesExplodeCMsg*>(msg);
    if (getDoesExplodeMsg) {
        getDoesExplodeMsg->doesExplode = GetDoesExplode();
        return;
    }

    SetDoesExplodeCMsg* setDoesExplodeCMsg = dynamic_cast<SetDoesExplodeCMsg*>(msg);
    if (setDoesExplodeCMsg) {
        SetDoesExplode(setDoesExplodeCMsg->doesExplode);
        return;
    }

    SpawnExplosionCMsg* spawnExplosionMsg = dynamic_cast<SpawnExplosionCMsg*>(msg);
    if (spawnExplosionMsg) {
        Entity* explosion = NewExplosion(vmake(mOwner->GetPos().x, mOwner->GetPos().y));
        gWorld->AddEntity(explosion);
        return;
    }
}