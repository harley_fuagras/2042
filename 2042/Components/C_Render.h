#pragma once
#include "ComponentMessage.h"

class Sprite;
class Entity;

class C_Render : public Component {
public:
    C_Render(std::vector<std::vector<std::string>> &texPaths, int animFps, bool loop, bool selfDestruct, Entity* owner);
    ~C_Render();

    void ReceiveMessage(ComponentMessage* msg);

private:
    Sprite* mSprite;

};
