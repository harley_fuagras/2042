#include "stdafx.h"
#include "lib/core.h"
#include "C_Collider.h"
#include "CollisionManager.h"

C_Collider::C_Collider(Entity* owner, float damage)
{
    mOwner = owner;
    mDamage = damage;
}

Entity* C_Collider::GetOwner() const {
    return mOwner;
}

vec2 C_Collider::GetPos() const {
    return vmake(mOwner->GetPos().x - mOwner->GetSize().x / 2, mOwner->GetPos().y + mOwner->GetSize().y / 2);
}

vec2 C_Collider::GetSize() const {
    return mOwner->GetSize();
}

float C_Collider::GetDamage() const {
    return mDamage;
}

void C_Collider::ReceiveMessage(ComponentMessage* msg) {
    GetColliderCMsg* getColliderMsg = dynamic_cast<GetColliderCMsg*>(msg);
    if (getColliderMsg) {
        getColliderMsg->collider = this;
        return;
    }
}
