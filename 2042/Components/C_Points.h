#pragma once
#include "Component.h"

class Entity;

class C_Points : public Component {
public:
    C_Points(int points, Entity* owner);
    void  Run();
    int   GetPoints() const;
    void  ReceiveMessage(ComponentMessage* msg);

private:
    int mPoints;

};
