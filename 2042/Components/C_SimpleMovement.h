#pragma once
#include "Entity.h"
#include "ComponentMessage.h"

class C_SimpleMovement : public Component {
public:
    C_SimpleMovement(vec2 velocity, bool isActive, Entity* owner);
    
    void Run();
    bool CheckOutOfBoundsDestruction(Entity::Type entityType);

    void SetVelocity(vec2 velocity);
    vec2 GetVelocity() const;
    void SetIsActive(bool isActive);
    bool GetIsActive() const;

    void ReceiveMessage(ComponentMessage* msg);

private:
    vec2 mVelocity;
    bool mIsActive;

};
