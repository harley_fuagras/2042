#pragma once
#include "IC_AI.h"
#include "Component.h"

class C_AI_HRand : public IC_AI {
public:
    C_AI_HRand(Entity* owner);
    virtual void Run();
    void ChangeDirection();

private:
    int mTicksToRoll;

};
