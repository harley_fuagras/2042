#include "stdafx.h"
#include "Globals.h"
#include "World.h"
#include "Entity.h"
#include "C_AI_ZigZag.h"
#include "ComponentMessage.h"

C_AI_ZigZag::C_AI_ZigZag(Entity* owner)
{
    mOwner = owner;
}

void C_AI_ZigZag::Run() {
    GetIsActiveCMsg getIsActiveMsg{};
    mOwner->ReceiveMessage(&getIsActiveMsg);
    if (getIsActiveMsg.isActive) {
        // Change direction when hitting level horizontal boundaries.
        if (mOwner->GetPos().x >= SCR_WIDTH || mOwner->GetPos().x <= 0) {
            GetVelocityCMsg getVelocityComponentMessage;
            mOwner->ReceiveMessage(&getVelocityComponentMessage);
            vec2 velocity = getVelocityComponentMessage.velocity;

            SetVelocityCMsg setVelocityMsg{ vmake(-getVelocityComponentMessage.velocity.x, getVelocityComponentMessage.velocity.y) };
            mOwner->ReceiveMessage(&setVelocityMsg);
        }
    }
}
