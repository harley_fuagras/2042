#include "stdafx.h"
#include "Action.h"
#include "ActionPrintMessage.h"

ActionPrintMessage::ActionPrintMessage(Entity* owner, const std::string& msg)
    : Action(owner), mMsg(msg)
{
}

void ActionPrintMessage::Start() {
}

void ActionPrintMessage::Update() {
    char buf[1024];
    sprintf_s(buf, "%s\n", mMsg.c_str());
    OutputDebugStringA(buf);
}

void ActionPrintMessage::End() {
}
