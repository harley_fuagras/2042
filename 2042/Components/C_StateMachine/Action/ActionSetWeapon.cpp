#include "stdafx.h"
#include "Action.h"
#include "ActionSetWeapon.h"
#include "ComponentMessage.h"

ActionSetWeapon::ActionSetWeapon(Entity* owner, const std::string& weaponId)
    : Action(owner), mWeaponId(weaponId)
{
}

void ActionSetWeapon::Start() {
    DisableWeaponsCMsg disableWeaponsMsg{};
    mOwner->ReceiveMessage(&disableWeaponsMsg);

    // If we set weaponId to empty string we will just diable all the weapons.
    if (mWeaponId != "") {
        EnableWeaponCMsg enableWeaponMsg{ mWeaponId };
        mOwner->ReceiveMessage(&enableWeaponMsg);
    }
}

void ActionSetWeapon::Update() {
}

void ActionSetWeapon::End() {
}
