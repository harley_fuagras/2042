#pragma once
#include "Action.h"
#include <string>

class Entity;

class ActionPrintMessage : public Action {
public:
    ActionPrintMessage(Entity* owner, const std::string& msg);

    virtual void Start()  override;
    virtual void Update() override;
    virtual void End()    override;

private:
    std::string mMsg;

};
