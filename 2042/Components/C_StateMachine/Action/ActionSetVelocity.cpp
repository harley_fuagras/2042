#include "stdafx.h"
#include "Action.h"
#include "ActionSetVelocity.h"
#include "ComponentMessage.h"

ActionSetVelocity::ActionSetVelocity(Entity* owner, const vec2& velocity)
    : Action(owner), mVelocity(velocity)
{
}

void ActionSetVelocity::Start() {
    SetVelocityCMsg setVelocityMsg{ mVelocity };
    mOwner->ReceiveMessage(&setVelocityMsg);
}

void ActionSetVelocity::Update() {
}

void ActionSetVelocity::End() {
}
