#pragma once
#include "Action.h"
#include "lib/core.h"

class Entity;

class ActionSetVelocity : public Action {

public:
    ActionSetVelocity(Entity* owner, const vec2& velocity);

    virtual void Start()  override;
    virtual void Update() override;
    virtual void End()    override;

private:
    vec2 mVelocity;

};
