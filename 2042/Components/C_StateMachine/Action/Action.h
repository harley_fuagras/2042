#pragma once

class Entity;

class Action {
public:
    Action(Entity* owner);
    virtual void Start() {};
    virtual void Update() {};
    virtual void End() {};

protected:
    Entity * mOwner;

};
