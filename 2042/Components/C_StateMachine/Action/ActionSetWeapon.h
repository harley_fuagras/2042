#pragma once
#include "Action.h"
#include "lib/core.h"
#include <string>

class Entity;

class ActionSetWeapon : public Action {

public:
    ActionSetWeapon(Entity* owner, const std::string& weaponId);

    virtual void Start()  override;
    virtual void Update() override;
    virtual void End()    override;

private:
    std::string mWeaponId;

};
