#include "stdafx.h"
#include "Globals.h"
#include "Transition.h"
#include "Condition.h"
#include "Action.h"

Transition::Transition(Condition* condition, const std::string& targetStateId)
    : mCondition(condition), mTargetStateId(targetStateId)
{
}

Transition::~Transition() {
    GAME_DELETE(mCondition);
    mCondition = nullptr;
}

bool Transition::CanTrigger() const {
    return mCondition->Check();
}

State* Transition::Trigger() {
    return mTargetState;
}

void Transition::SetTargetState(State* targetState) {
    mTargetState = targetState;
}

State* Transition::GetTargetState() const {
    return mTargetState;
}

const std::string& Transition::GetTargetStateId() const {
    return mTargetStateId;
}
