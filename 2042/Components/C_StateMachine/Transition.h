#pragma once
#include <string>

class Condition;
class State;

class Transition {
public:
    Transition(Condition* condition, const std::string& targetStateId);
    ~Transition();

    bool CanTrigger() const;
    State* Trigger();

    void SetTargetState(State* targetState);
    State* GetTargetState() const;
    const std::string& GetTargetStateId() const;

private:
    Condition*  mCondition;
    State*      mTargetState;
    std::string mTargetStateId;

};
