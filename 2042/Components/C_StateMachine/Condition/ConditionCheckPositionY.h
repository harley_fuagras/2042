#pragma once
#include "Condition.h"

class ConditionCheckPositionY : public Condition {
public:
    ConditionCheckPositionY(Entity* owner, float positionY, bool triggerWhenBelow);
    virtual bool Check() const override;

private:
    float mPositionY;
    bool  mTriggerWhenBelow;

};
