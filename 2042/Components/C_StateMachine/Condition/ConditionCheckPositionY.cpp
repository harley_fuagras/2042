#include "stdafx.h"
#include "Entity.h"
#include "ConditionCheckPositionY.h"

ConditionCheckPositionY::ConditionCheckPositionY(Entity* owner, float positionY, bool triggerWhenBelow)
    : Condition(owner)
{
    mPositionY        = positionY;
    mTriggerWhenBelow = triggerWhenBelow;
}

bool ConditionCheckPositionY::Check() const {
    if (mTriggerWhenBelow)
        return mOwner->GetPos().y < mPositionY;
    else
        return mOwner->GetPos().y > mPositionY;
}
