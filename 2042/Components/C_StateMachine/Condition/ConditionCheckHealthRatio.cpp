#include "stdafx.h"
#include "ConditionCheckHealthRatio.h"
#include "ComponentMessage.h"
#include "Entity.h"

ConditionCheckHealthRatio::ConditionCheckHealthRatio(Entity* owner, float triggerRatio, bool triggerWhenLess)
    : Condition(owner), mTriggerRatio(triggerRatio), mTriggerWhenLess(triggerWhenLess)
{
}

bool ConditionCheckHealthRatio::Check() const {
    
    GetCurrentHealthRatioCMsg getCurrentHealthRatioMsg = {};
    mOwner->ReceiveMessage(&getCurrentHealthRatioMsg);
    
    if (mTriggerWhenLess)
        return getCurrentHealthRatioMsg.ratio < mTriggerRatio;
    else
        return getCurrentHealthRatioMsg.ratio > mTriggerRatio;
}
