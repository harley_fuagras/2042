#include "stdafx.h"
#include "Condition.h"
#include "ConditionNot.h"

ConditionNot::ConditionNot(Entity* owner, Condition* c)
    : Condition(owner), m_condition(c)
{
}

ConditionNot::~ConditionNot() {
    delete m_condition;
}

bool ConditionNot::Check() const {
    return !m_condition->Check();
}
