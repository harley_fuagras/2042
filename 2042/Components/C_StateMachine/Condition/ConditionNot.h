#pragma once
#include "Condition.h"

class ConditionNot : public Condition {
public:
    ConditionNot(Entity* owner, Condition* c1);
    ~ConditionNot();

    bool Check() const;

private:
    Condition* m_condition;

};
