#pragma once
#include "Condition.h"

class ConditionCheckHealthRatio : public Condition {
public:
    ConditionCheckHealthRatio(Entity* owner, float triggerRatio, bool triggerWhenLess);
    virtual bool Check() const override;

private:
    float mTriggerRatio;
    bool  mTriggerWhenLess;

};
