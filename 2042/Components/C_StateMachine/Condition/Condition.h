#pragma once

class Entity;

class Condition {
public:
    Condition(Entity* owner);
    virtual ~Condition();
    virtual bool Check() const = 0;

protected:
    Entity * mOwner;

};
