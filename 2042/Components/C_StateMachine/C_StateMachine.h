#pragma once
#include "Component.h"
#include <vector>
#include <string>
#include "lib/rapidjson/document.h"
#include "lib/rapidjson/filereadstream.h"
#include "lib/rapidjson/stringbuffer.h"

class State;
class Condition;
class Action;

class C_StateMachine : public Component {
public:
    C_StateMachine(Entity* owner, const std::string& filename);
    ~C_StateMachine();
    
    void Start();
    void Load(const std::string& filename);
    Condition* CreateCondition(const std::string& conditionId, rapidjson::Value::ConstValueIterator transitionIterator);
    Action* CreateAction(const std::string& actionId, rapidjson::Value::ConstValueIterator actionIterator);
    State* FindState(const std::string& stateId) const;

    virtual void Run() override;

private:
    std::vector<State*> mStates;
    State*              mCurrentState;
    bool                mIsInitialized;

};
