#pragma once
#include <string>
#include <vector>

class Action;
class Transition;
class Entity;

class State {
public:
    State(Entity* owner, const std::string& id);
    ~State();

    virtual void OnEnter();
    virtual void Update();
    virtual void OnExit();

    Entity* GetOwner() const;
    void SetOwner(Entity* owner);
    const std::vector<Transition*> GetTransitions();
    const std::string& GetId() const;

    void AddEnterAction(Action* action);
    void AddStateAction(Action* action);
    void AddExitAction(Action* action);

    void AddTransition(Transition* transition);

protected:
    Entity*                  mOwner;
    std::vector<Action*>     mEnterActions;
    std::vector<Action*>     mStateActions;
    std::vector<Action*>     mExitActions;
    std::vector<Transition*> mTransitions;
    std::string              mId;
};
