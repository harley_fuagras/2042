#include "stdafx.h"
#include "Globals.h"
#include "Action.h"
#include "Transition.h"
#include "State.h"

State::State(Entity* owner, const std::string& id)
    : mOwner(owner), mId(id)
{
}

State::~State() {
    // Delete actions.
    for (auto& enterAction : mEnterActions) {
        GAME_DELETE(enterAction);
        enterAction = nullptr;
    }

    for (auto& stateAction : mStateActions) {
        GAME_DELETE(stateAction);
        stateAction = nullptr;
    }

    for (auto& exitAction : mExitActions) {
        GAME_DELETE(exitAction);
        exitAction = nullptr;
    }

    // Delete transitions.
    for (Transition* transition : mTransitions)
    {
        GAME_DELETE(transition);
        transition = nullptr;
    }
}

void State::OnEnter() {
    if (!mEnterActions.empty())
        for (auto& enterAction : mEnterActions)
            enterAction->Start();

    if (!mStateActions.empty())
        for (auto& stateAction : mStateActions)
            stateAction->Start();
}

void State::Update() {
    if (!mStateActions.empty())
        for (auto& stateAction : mStateActions)
            stateAction->Update();
}

void State::OnExit() {
    if (!mStateActions.empty())
        for (auto& stateAction : mStateActions)
            stateAction->End();

    if (!mExitActions.empty())
        for (auto& exitAction : mExitActions)
            exitAction->Start();
}

Entity* State::GetOwner() const {
    return mOwner;
}

void State::SetOwner(Entity* owner) {
    mOwner = owner;
}

const std::vector<Transition*> State::GetTransitions() {
    return mTransitions;
}

const std::string& State::GetId() const {
    return mId;
}

void State::AddEnterAction(Action* action) {
    mEnterActions.push_back(action);
}

void State::AddStateAction(Action* action) {
    mStateActions.push_back(action);
}

void State::AddExitAction(Action* action) {
    mExitActions.push_back(action);
}

void State::AddTransition(Transition* transition) {
    mTransitions.push_back(transition);
}
