#include "stdafx.h"
#include "lib/core.h"
#include "Globals.h"
#include "C_StateMachine.h"
#include "State.h"
#include "Transition.h"
#include "Condition.h"
#include "ConditionCheckPositionY.h"
#include "ConditionCheckHealthRatio.h"
#include "Action.h"
#include "ActionPrintMessage.h"
#include "ActionSetVelocity.h"
#include "ActionSetWeapon.h"

C_StateMachine::C_StateMachine(Entity* owner, const std::string& filename) {
    mOwner = owner;
    Load(filename);
}

C_StateMachine::~C_StateMachine() {
    for (State* state : mStates) {
        GAME_DELETE(state);
        state = nullptr;
    }
}

void C_StateMachine::Load(const std::string& filename) {
    FILE* stateMachineFile;
    fopen_s(&stateMachineFile, filename.c_str(), "rb");
    char stateMachineBuffer[65536];
    rapidjson::FileReadStream stateMachineStream(stateMachineFile, stateMachineBuffer, sizeof(stateMachineBuffer));
    rapidjson::Document stateDocuments;
    stateDocuments.ParseStream(stateMachineStream);

    for (auto& stateDocument : stateDocuments.GetObject()) {
        State* state = GAME_NEW(State, (mOwner, stateDocument.name.GetString()));
        mStates.push_back(state);

        // Parse transitions.
        const rapidjson::Value& transitions = stateDocument.value["transitions"];
        if (!transitions.Empty()) {
            for (rapidjson::Value::ConstValueIterator itr = transitions.Begin(); itr != transitions.End(); ++itr) {
                std::string conditionId = (*itr)["condition"]["id"].GetString();
                Condition* condition = CreateCondition(conditionId, itr);
                Transition* transition = GAME_NEW(Transition, (condition, (*itr)["target_state_id"].GetString()));
                state->AddTransition(transition);
            }
        }

        // Parse actions.
        const rapidjson::Value& enterActions = stateDocument.value["enter_actions"];
        if (!enterActions.Empty()) {
            for (rapidjson::Value::ConstValueIterator itr = enterActions.Begin(); itr != enterActions.End(); ++itr) {
                std::string actionId = (*itr)["id"].GetString();
                Action* action = CreateAction(actionId, itr);
                state->AddEnterAction(action);
            }
        }

        const rapidjson::Value& stateActions = stateDocument.value["state_actions"];
        if (!stateActions.Empty()) {
            for (rapidjson::Value::ConstValueIterator itr = stateActions.Begin(); itr != stateActions.End(); ++itr) {
                std::string actionId = (*itr)["id"].GetString();
                Action* action = CreateAction(actionId, itr);
                state->AddStateAction(action);
            }
        }

        const rapidjson::Value& exitActions = stateDocument.value["exit_actions"];
        if (!exitActions.Empty()) {
            for (rapidjson::Value::ConstValueIterator itr = exitActions.Begin(); itr != exitActions.End(); ++itr) {
                std::string actionId = (*itr)["id"].GetString();
                Action* action = CreateAction(actionId, itr);
                state->AddExitAction(action);
            }
        }

        // Add target state to every transition.
        for (State* state : mStates) {
            for (Transition* transition : state->GetTransitions()) {
                std::string targetStateId = transition->GetTargetStateId();
                State* targetState = FindState(targetStateId);
                transition->SetTargetState(targetState);
            }
        }
    }
}

Condition* C_StateMachine::CreateCondition(const std::string& conditionId, rapidjson::Value::ConstValueIterator transitionIterator) {
    Condition* condition = nullptr;
    if (conditionId == "check_screen_pos_y") {
        condition = GAME_NEW(ConditionCheckPositionY, (
            mOwner,
            (*transitionIterator)["condition"]["max_pos_y"].GetFloat(),
            (*transitionIterator)["condition"]["trigger_when_below"].GetBool()
        ));
    } else if (conditionId == "check_health_ratio") {
        condition = GAME_NEW(ConditionCheckHealthRatio, (
            mOwner,
            (*transitionIterator)["condition"]["ratio_trigger"].GetFloat(),
            (*transitionIterator)["condition"]["trigger_when_less"].GetBool()
        ));
    }

    return condition;
}

Action* C_StateMachine::CreateAction(const std::string& actionId, rapidjson::Value::ConstValueIterator actionIterator) {
    Action* action = nullptr;
    if (actionId == "print_message") {
        action = GAME_NEW(ActionPrintMessage, (
            mOwner,
            (*actionIterator)["msg"].GetString()
        ));
    } else if (actionId == "set_velocity") {
        action = GAME_NEW(ActionSetVelocity, (
            mOwner,
            vmake(
                (*actionIterator)["velocity_x"].GetFloat(),
                (*actionIterator)["velocity_y"].GetFloat()
            )
        ));
    } else if (actionId == "set_weapon") {
         action = GAME_NEW(ActionSetWeapon, (
             mOwner,
             (*actionIterator)["weapon_id"].GetString()
         ));
    }

    return action;
}

State* C_StateMachine::FindState(const std::string& stateId) const {
    for (State* state : mStates) {
        if (state->GetId() == stateId)
            return state;
    }
    return nullptr;
}

void C_StateMachine::Start() {
    // Note: We assume that the first state of the vector is the starting current state.
    mCurrentState = mStates.at(0);
    mIsInitialized = true;
    if (mCurrentState)
        mCurrentState->OnEnter();
}

void C_StateMachine::Run() {
    if (mIsInitialized) {
        mCurrentState->Update();
        const std::vector<Transition*> transitions = mCurrentState->GetTransitions();
        for (Transition* transition : transitions) {
            if (transition->CanTrigger()) {
                mCurrentState->OnExit();
                State* nextState = transition->Trigger();
                nextState->OnEnter();
                mCurrentState = nextState;
                return;
            }
        }
    }
}
