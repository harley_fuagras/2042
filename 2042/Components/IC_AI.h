#pragma once
#include "Component.h"
#include <map>

class IC_AI : public Component {
public:
    enum Type {
        AI_ZIGZAG,
        AI_HRAND,
    };

    virtual ~IC_AI() {};
    virtual void Run() = 0;

};

static const std::map<std::string, IC_AI::Type> AITypes = {
    { "ZigZag", IC_AI::Type::AI_ZIGZAG },
    { "HRand",  IC_AI::Type::AI_HRAND },
};
