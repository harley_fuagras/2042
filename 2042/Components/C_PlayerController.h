#pragma once
#include "lib/core.h"
#include "Globals.h"
#include "Component.h"
#include "EventManager.h"
#include <map>

class C_PlayerController : public Component, public EventManager::Listener{
public:

    enum Pitch {
        P_NONE,
        P_LEFT,
        P_RIGHT,
    };

    std::map<Pitch, int> pitchMap {
        { P_NONE,  0 },
        { P_LEFT,  1 },
        { P_RIGHT, 2 },
    };

    C_PlayerController(vec2 velocity, int shootCooldown, Entity* owner);
    ~C_PlayerController();
    
    void Run();
    void Move(Direction dir);
    void Shoot();

    void SetVelocity(vec2 velocity);
    void SetIsActive(bool isActive);
    vec2 GetVelocity() const;
    bool GetIsActive() const;

    void ReceiveMessage(ComponentMessage* msg);

    virtual bool ProcessEvent(const EventManager::CEvent& e);

private:
    int   mShootCooldown;
    int   mCurrentShootCooldown; // Ticks.
    vec2  mVelocity;
    Pitch mPitch;
    bool  mIsActive;

};
