#pragma once
#include "lib/core.h"
#include "Globals.h"
#include "C_Faction.h"
#include "C_PlayerController.h"
#include "Sprite.h"

class C_Collider;

struct ComponentMessage {
public:
    virtual ~ComponentMessage() = 0 {};
};

struct SetCurrentHealthCMsg : public ComponentMessage {
    SetCurrentHealthCMsg(float _health) {
        health = _health;
    }
    float health;
};

struct HurtCMsg : public ComponentMessage {
    HurtCMsg(float _damage) {
        damage = _damage;
    }
    float damage;
};

struct SetWillBeDestroyedCMsg : public ComponentMessage {
    SetWillBeDestroyedCMsg(bool _willBeDestroyed, bool _checkPoints) {
        willBeDestroyed = _willBeDestroyed;
        checkPoints = _checkPoints;
    }
    bool willBeDestroyed;
    bool checkPoints;
};

struct SetDoesExplodeCMsg : public ComponentMessage {
    SetDoesExplodeCMsg( bool _doesExplode) { doesExplode = _doesExplode; }
    bool doesExplode;
};

struct GetWillBeDestroyedCMsg : public ComponentMessage {
    bool willBeDestroyed;
};

struct GetDoesExplodeCMsg : public ComponentMessage {
    bool doesExplode;
};

struct SetFactionCMsg : public ComponentMessage {
    SetFactionCMsg(C_Faction::Type _typeFaction) { typeFaction = _typeFaction; }
    C_Faction::Type typeFaction;
};

struct GetFactionCMsg : public ComponentMessage {
    C_Faction::Type typeFaction;
};

struct MoveCMsg : public ComponentMessage {
    MoveCMsg(Direction _direction) { direction = _direction; }
    Direction direction;
};

struct ShootCMsg : public ComponentMessage {
    ShootCMsg() {}
};

struct SetIsActiveControllerCMsg : public ComponentMessage {
    SetIsActiveControllerCMsg(bool _isActive) { isActive = _isActive; }
    bool isActive;
};

struct GetIsActiveControllerCMsg : public ComponentMessage {
    bool isActive;
};

struct GetPlayerControllerCMsg : public ComponentMessage {
    C_PlayerController* playerController;
};

struct SetVelocityCMsg : public ComponentMessage {
    SetVelocityCMsg(vec2 _veloctiy) { velocity = _veloctiy; }
    vec2 velocity;
};

struct GetVelocityCMsg : public ComponentMessage {
    vec2 velocity;
};

struct SetIsActiveCMsg : public ComponentMessage {
    SetIsActiveCMsg(bool _isActive) { isActive = _isActive; }
    bool isActive;
};

struct GetIsActiveCMsg : public ComponentMessage {
    bool isActive;
};

struct GetColliderCMsg : public ComponentMessage {
    C_Collider* collider;
};

struct SetSpriteRowCMsg : public ComponentMessage {
    SetSpriteRowCMsg(int _spriteRow) { spriteRow = _spriteRow; }
    int spriteRow;
};

struct GetSpriteRowCMsg : public ComponentMessage {
    int spriteRow;
};

struct SetSpriteCMsg : public ComponentMessage {
    SetSpriteCMsg(Sprite* _sprite) { sprite = _sprite; }
    Sprite* sprite;
};

struct GetSpriteCMsg : public ComponentMessage {
    Sprite* sprite;
};

struct SpawnExplosionCMsg : public ComponentMessage {};

struct GetPointsCMsg : public ComponentMessage {
    int points;
};

struct DisableWeaponsCMsg : public ComponentMessage {
    DisableWeaponsCMsg() {}
};

struct EnableWeaponCMsg : public ComponentMessage {
    EnableWeaponCMsg(const std::string& _weaponId) { weaponId = _weaponId; }
    std::string weaponId;
};

struct GetCurrentHealthRatioCMsg : public ComponentMessage {
    float ratio;
};
