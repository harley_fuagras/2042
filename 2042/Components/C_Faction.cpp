#include "stdafx.h"
#include "C_Faction.h"
#include "ComponentMessage.h"

C_Faction::C_Faction(Type typeFaction, Entity* owner)
    : mTypeFaction(typeFaction)
{
    mOwner = owner;
}

void C_Faction::SetTypeFaction(Type typeFaction) {
    mTypeFaction = typeFaction;
}

C_Faction::Type C_Faction::GetTypeFaction() const {
    return mTypeFaction;
}

void C_Faction::ReceiveMessage(ComponentMessage* msg) {
    SetFactionCMsg *setFactionMsg = dynamic_cast<SetFactionCMsg*>(msg);
    if (setFactionMsg) {
        SetTypeFaction(setFactionMsg->typeFaction);	
        return;
    }

    GetFactionCMsg *getFactionMsg = dynamic_cast<GetFactionCMsg*>(msg);
    if (getFactionMsg) {
        getFactionMsg->typeFaction = GetTypeFaction();
        return;
    }
}
