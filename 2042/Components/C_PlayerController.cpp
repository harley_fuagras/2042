#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "C_PlayerController.h"
#include "Entity.h"
#include "ComponentMessage.h"
#include "World.h"
#include "SoundManager.h"

C_PlayerController::C_PlayerController(vec2 velocity, int shootCooldown, Entity* owner)
    : mVelocity(velocity), mShootCooldown(shootCooldown), mCurrentShootCooldown(0), mPitch(P_NONE), mIsActive(true)
{
    mOwner = owner;
    gEventManager->Register(this);
}

C_PlayerController::~C_PlayerController() {
    gEventManager->Unregister(this);
}

void C_PlayerController::Run() {
    // Update shoot cooldown.
    if (mCurrentShootCooldown != 0)
        mCurrentShootCooldown--;

    // Send message to change pitch.
    SetSpriteRowCMsg setSpriteRowMsg = { pitchMap.find(mPitch)->second };
    mOwner->ReceiveMessage(&setSpriteRowMsg);

    // End level out of screen movement.
    if (!mIsActive) {
        mOwner->SetPos(vadd(mOwner->GetPos(), mVelocity));
        //Check if player abandons screen to change level.
        if (mOwner->GetPos().y - (mOwner->GetSize().y / 2) > SCR_HEIGHT)
            gWorld->SetWillChangeLevel(true);
    }
}

void C_PlayerController::Move(Direction  dir) {
    switch (dir) {
        case DIR_LEFT:
            mOwner->SetPos(vmake(mOwner->GetPos().x - mVelocity.x, (mOwner->GetPos().y)));
            if (mOwner->GetPos().x - (mOwner->GetSize().x / 2.0f) <= 0)
                mOwner->SetPos(vmake(mOwner->GetSize().x / 2.0f, mOwner->GetPos().y));
            mPitch = P_LEFT;
            break;
        case DIR_RIGHT:
            mOwner->SetPos(vmake(mOwner->GetPos().x + mVelocity.x, mOwner->GetPos().y));
            if (mOwner->GetPos().x + (mOwner->GetSize().x / 2.0f) >= SCR_WIDTH)
                mOwner->SetPos(vmake(SCR_WIDTH - (mOwner->GetSize().x / 2.0f), mOwner->GetPos().y));
            mPitch = P_RIGHT;
            break;
        case DIR_DOWN:
            mOwner->SetPos(vmake(mOwner->GetPos().x, mOwner->GetPos().y - mVelocity.y));
            if (mOwner->GetPos().y - (mOwner->GetSize().y / 2.0f) <= 0)
                mOwner->SetPos(vmake(mOwner->GetPos().x, mOwner->GetSize().y / 2.0f));
            mPitch = P_NONE;
            break;
        case DIR_UP:
            mOwner->SetPos(vmake(mOwner->GetPos().x, mOwner->GetPos().y + mVelocity.y));
            if (mOwner->GetPos().y + (mOwner->GetSize().y / 2.0f) >= SCR_HEIGHT)
                mOwner->SetPos(vmake(mOwner->GetPos().x, SCR_HEIGHT - (mOwner->GetSize().y / 2.0f)));
            mPitch = P_NONE;
            break;
        case DIR_NONE:
            mPitch = P_NONE;
            break;
    }
}

void C_PlayerController::Shoot() {
    if (mIsActive) {
        if (mCurrentShootCooldown == 0) {
            mCurrentShootCooldown = mShootCooldown;
            gWorld->AddEntity(NewBullet(mOwner->GetPos(), PLAYER_BULLET_VELOCITY, PLAYER_BULLET_DAMAGE, C_Faction::Type::F_PLAYER));
            std::string soundKey = FX_LASER_KEY;
            gSoundManager->PlaySound(soundKey);
        }
    }
}

void C_PlayerController::SetVelocity(vec2 velocity) {
    mVelocity = velocity;
}

void C_PlayerController::SetIsActive(bool isActive) {
    mIsActive = isActive;
}

vec2 C_PlayerController::GetVelocity() const {
    return mVelocity;
}

bool C_PlayerController::GetIsActive() const {
    return mIsActive;
}

void C_PlayerController::ReceiveMessage(ComponentMessage* msg) {
    MoveCMsg *moveMsg = dynamic_cast<MoveCMsg*>(msg);
    if (moveMsg) {
        Move(moveMsg->direction);
        return;
    }

    ShootCMsg *shootMsg = dynamic_cast<ShootCMsg*>(msg);
    if (shootMsg) {
        Shoot();
        return;
    }

    SetIsActiveControllerCMsg *setIsActiveMsg = dynamic_cast<SetIsActiveControllerCMsg*>(msg);
    if (setIsActiveMsg) {
        SetIsActive(setIsActiveMsg->isActive);
        return;
    }

    GetIsActiveControllerCMsg *getIsActiveMsg = dynamic_cast<GetIsActiveControllerCMsg*>(msg);
    if (getIsActiveMsg) {
        getIsActiveMsg->isActive = GetIsActive();
        return;
    }

    GetPlayerControllerCMsg *getPlayerControllerMsg = dynamic_cast<GetPlayerControllerCMsg*>(msg);
    if (getPlayerControllerMsg) {
        getPlayerControllerMsg->playerController = this;
        return;
    }

    SetVelocityCMsg *setVelocityMsg = dynamic_cast<SetVelocityCMsg*>(msg);
    if (setVelocityMsg) {
        SetVelocity(setVelocityMsg->velocity);
        return;
    }

    GetVelocityCMsg *getVelocityMsg = dynamic_cast<GetVelocityCMsg*>(msg);
    if (getVelocityMsg) {
        getVelocityMsg->velocity = GetVelocity();
        return;
    }
}

bool C_PlayerController::ProcessEvent(const EventManager::CEvent& e) {
    EventManager::TEvent eventType = e.GetType();
    switch (eventType) {
        case EventManager::TEvent::E_KEY_UP: {
            Move(DIR_UP);
            return true;
        }
        case EventManager::TEvent::E_KEY_LEFT: {
            Move(DIR_LEFT);
            return true;
        }
        case EventManager::TEvent::E_KEY_DOWN: {
            Move(DIR_DOWN);
            return true;
        }
        case EventManager::TEvent::E_KEY_RIGHT: {
            Move(DIR_RIGHT);
            return true;
        }
        case EventManager::TEvent::E_KEY_SPACE: {
            Shoot();
            return true;
        }
    }
    return false;
}
