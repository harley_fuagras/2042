#include "stdafx.h"
#include "C_Render.h"
#include "Sprite.h"
#include "GraphicEngine.h"

C_Render::C_Render(std::vector<std::vector<std::string>> &texPaths, int animFps, bool loop, bool selfDestruct, Entity* owner) {
    mOwner  = owner;
    mSprite = GAME_NEW(Sprite, (owner, texPaths, animFps, loop, selfDestruct));
    gGraphicEngine->AddSprite(mSprite);
}

C_Render::~C_Render() {
    gGraphicEngine->RemoveSprite(mSprite);
    GAME_DELETE(mSprite);
    mSprite = nullptr;
}

void C_Render::ReceiveMessage(ComponentMessage* msg) {
    SetSpriteRowCMsg *setSpriteRowMsg = dynamic_cast<SetSpriteRowCMsg*>(msg);
    if (setSpriteRowMsg) {
        mSprite->SetCurrentGfxRow(setSpriteRowMsg->spriteRow);
        return;
    }

    GetSpriteRowCMsg *getSpriteRowMsg = dynamic_cast<GetSpriteRowCMsg*>(msg);
    if (getSpriteRowMsg) {
        getSpriteRowMsg->spriteRow = mSprite->GetCurrentGfxRow();
        return;
    }

    GetSpriteCMsg *getSpriteMsg = dynamic_cast<GetSpriteCMsg*>(msg);
    if (getSpriteMsg) {
        getSpriteMsg->sprite = mSprite;
        return;
    }
}
