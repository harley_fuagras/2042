#include "stdafx.h"
#include "Globals.h"
#include "World.h"
#include "C_AI_HRand.h"
#include "Entity.h"
#include "ComponentMessage.h"

C_AI_HRand::C_AI_HRand(Entity* owner)
    : mTicksToRoll(ENEMY_HRAND_COOLDOWN)
{
    mOwner = owner;
}

void C_AI_HRand::Run() {
    GetIsActiveCMsg getIsActiveMsg{};
    mOwner->ReceiveMessage(&getIsActiveMsg);
    if (getIsActiveMsg.isActive) {
        
        if (mTicksToRoll == 0) {
            if (GetRandInt(0, 1))
                ChangeDirection();
            mTicksToRoll = ENEMY_HRAND_COOLDOWN;
        } else
            mTicksToRoll--;

        // Change direction when hitting level horizontal boundaries.
        if (mOwner->GetPos().x >= SCR_WIDTH || mOwner->GetPos().x <= 0)
            ChangeDirection();
    }
}

void C_AI_HRand::ChangeDirection() {
    GetVelocityCMsg getVelocityComponentMessage;
    mOwner->ReceiveMessage(&getVelocityComponentMessage);
    vec2 velocity = getVelocityComponentMessage.velocity;

    SetVelocityCMsg setVelocityMsg{ vmake(-getVelocityComponentMessage.velocity.x, getVelocityComponentMessage.velocity.y) };
    mOwner->ReceiveMessage(&setVelocityMsg);
}
