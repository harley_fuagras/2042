#pragma once
#include "C_Weapon.h"

class C_WeaponMultiple : public C_Weapon {
public:
    C_WeaponMultiple(
        std::string id, int cooldown, int chance, float damage, float bulletSpeed,
        float angleSeparation, int bulletsNumber, Entity* owner
    );
    virtual void Run() override;
    virtual void Shoot() override;
    virtual void ReceiveMessage(ComponentMessage* msg) override;

private:
    float mAngleSeparation;
    int   mBulletsNumber;
};
