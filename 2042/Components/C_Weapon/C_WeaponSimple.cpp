#include "stdafx.h"
#include "C_WeaponSimple.h"
#include "Globals.h"
#include "World.h"
#include "Entity.h"

C_WeaponSimple::C_WeaponSimple(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner)
    : C_Weapon(id, cooldown, chance, damage, bulletSpeed, owner)
{
}

void C_WeaponSimple::Run() {
    C_Weapon::Run();
}

void C_WeaponSimple::Shoot() {
    C_Weapon::Shoot();
    gWorld->AddEntity(NewBullet(
        mOwner->GetPos(),
        vmake(0.f, -mBulletSpeed),
        mDamage, 
        C_Faction::Type::F_ENEMY
    ));
}

void C_WeaponSimple::ReceiveMessage(ComponentMessage* msg) {
    C_Weapon::ReceiveMessage(msg);
}
