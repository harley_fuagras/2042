#include "stdafx.h"
#include "Globals.h"
#include "C_Weapon.h"
#include "Game.h"
#include "World.h"
#include "AppManager.h"
#include "GraphicEngine.h"
#include "Sprite.h"
#include "ComponentMessage.h"
#include "Entity.h"
#include "SoundManager.h"
#include "C_WeaponSimple.h"
#include "C_WeaponDirected.h"
#include "C_WeaponRadial.h"
#include "C_WeaponMultiple.h"
#include <map>

C_Weapon::C_Weapon(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner)
    : mId(id), mCooldown(cooldown), mCurrentCooldown(cooldown), mChance(chance), mDamage(damage), mBulletSpeed(bulletSpeed)
{
    mOwner    = owner;
    mIsActive = true;
}

void C_Weapon::Run() {
    if (mIsActive)
    {
        if (mCurrentCooldown > 0) {
            mCurrentCooldown--;
        } else {
            mCurrentCooldown = mCooldown;
            if (GetRandInt(0, 100) < mChance)
                Shoot();
        }
    }
}

void C_Weapon::Shoot() {
    std::string soundKey = FX_LASER_KEY;
    gSoundManager->PlaySound(soundKey);
}

void C_Weapon::ReceiveMessage(ComponentMessage* msg) {
    ShootCMsg* shootMsg = dynamic_cast<ShootCMsg*>(msg);
    if (shootMsg) {
        Shoot();
        return;
    }

    DisableWeaponsCMsg* disableWeaponsMsg = dynamic_cast<DisableWeaponsCMsg*>(msg);
    if (disableWeaponsMsg) {
        mIsActive = false;
        return;
    }

    EnableWeaponCMsg* enableWeaponMsg = dynamic_cast<EnableWeaponCMsg*>(msg);
    if (enableWeaponMsg && enableWeaponMsg->weaponId == mId) {
        mIsActive = true;
        return;
    }
}

C_Weapon* C_Weapon::Load(const std::string& weaponId, Entity* entity) {
    Game::WeaponData* weaponData = gGame->GetWeaponsData().at(weaponId);
    C_Weapon::Type type = mWeaponTypes.at(weaponData->type);

    switch (type) {
        case W_NONE: {
            return nullptr;
            break;
        }
        case W_SIMPLE: {
            return GAME_NEW(C_WeaponSimple, (
                weaponData->id,
                weaponData->cooldown,
                weaponData->chance,
                weaponData->damage,
                weaponData->bulletSpeed,
                entity
            ));
            break;
        }
        case W_DIRECTED: {
            return GAME_NEW(C_WeaponDirected, (
                weaponData->id,
                weaponData->cooldown,
                weaponData->chance,
                weaponData->damage,
                weaponData->bulletSpeed,
                entity
            ));
            break;
        }
        case W_RADIAL: {
            Game::WeaponRadialData* weaponRadialData = dynamic_cast<Game::WeaponRadialData*>(weaponData);
            CUSTOM_ASSERT(weaponRadialData, "Invalid Radial Weapon");
            
            return GAME_NEW(C_WeaponRadial, (
                weaponData->id,
                weaponRadialData->cooldown,
                weaponRadialData->chance,
                weaponRadialData->damage,
                weaponRadialData->bulletSpeed,
                weaponRadialData->initialAngle,
                weaponRadialData->angleVariation,
                weaponRadialData->bulletsNumber,
                entity
            ));
            break;
        }
        case W_MULTIPLE: {
            Game::WeaponMultipleData* weaponMultipleData = dynamic_cast<Game::WeaponMultipleData*>(weaponData);
            CUSTOM_ASSERT(weaponMultipleData, "Invalid Multiple Weapon");

            return GAME_NEW(C_WeaponMultiple, (
                weaponData->id,
                weaponMultipleData->cooldown,
                weaponMultipleData->chance,
                weaponMultipleData->damage,
                weaponMultipleData->bulletSpeed,
                weaponMultipleData->angleSeparation,
                weaponMultipleData->bulletsNumber,
                entity
            ));
            break;
        }
        default: {
            return GAME_NEW(C_WeaponSimple, (
                weaponData->id,
                weaponData->cooldown,
                weaponData->chance,
                weaponData->damage,
                weaponData->bulletSpeed,
                entity
            ));
            break;
        }
    }

    return nullptr;
}

const std::map<std::string, C_Weapon::Type> C_Weapon::mWeaponTypes = {
    { "",          C_Weapon::Type::W_NONE },
    { "simple",    C_Weapon::Type::W_SIMPLE },
    { "directed",  C_Weapon::Type::W_DIRECTED },
    { "radial",    C_Weapon::Type::W_RADIAL },
    { "multiple",  C_Weapon::Type::W_MULTIPLE },
};
