#include "stdafx.h"
#include "C_WeaponDirected.h"
#include "Globals.h"
#include "World.h"
#include "Entity.h"

C_WeaponDirected::C_WeaponDirected(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner)
    : C_Weapon(id, cooldown, chance, damage, bulletSpeed, owner)
{
}

void C_WeaponDirected::Run() {
    C_Weapon::Run();
}

void C_WeaponDirected::Shoot() {
    C_Weapon::Shoot();
    vec2 dir = vnorm(vsub(gWorld->GetPlayer()->GetPos(), mOwner->GetPos()));
    vec2 velocity = vscale(dir, mBulletSpeed);
    gWorld->AddEntity(NewBullet(mOwner->GetPos(), velocity, mDamage, C_Faction::Type::F_ENEMY));
}

void C_WeaponDirected::ReceiveMessage(ComponentMessage* msg) {
    C_Weapon::ReceiveMessage(msg);
}
