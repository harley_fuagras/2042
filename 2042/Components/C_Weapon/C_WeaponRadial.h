#pragma once
#include "C_Weapon.h"

class C_WeaponRadial : public C_Weapon {
public:
    C_WeaponRadial(
        std::string id, int cooldown, int chance, float damage, float bulletSpeed, 
        float initialAngle, float angleVariation, int bulletsNumber, Entity* owner
    );
    virtual void Run() override;
    virtual void Shoot() override;
    virtual void ReceiveMessage(ComponentMessage* msg) override;

private:
    float mInitialAngle;
    float mAngleVariation;
    int   mBulletsNumber;
};
