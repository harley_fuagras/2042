#include "stdafx.h"
#include "C_WeaponRadial.h"
#include "Globals.h"
#include "World.h"
#include "Entity.h"

C_WeaponRadial::C_WeaponRadial(
    std::string id, int cooldown, int chance, float damage, float bulletSpeed,
    float initialAngle, float angleVariation, int bulletsNumber, Entity* owner
)
    : C_Weapon(id, cooldown, chance, damage, bulletSpeed, owner), 
    mInitialAngle(initialAngle), mAngleVariation(angleVariation), mBulletsNumber(bulletsNumber)
{
}

void C_WeaponRadial::Run() {
    C_Weapon::Run();
}

void C_WeaponRadial::Shoot() {
    C_Weapon::Shoot();

    vec2 dir = vrotate(vmake(0, 1), mInitialAngle);
    float angleDiff = DEG2RAD(360.f / mBulletsNumber);

    for (int i = 0; i < mBulletsNumber; ++i)
    {
        vec2 velocity = vscale(dir, mBulletSpeed);
        gWorld->AddEntity(NewBullet(mOwner->GetPos(), velocity, mDamage, C_Faction::Type::F_ENEMY));

        // Rotate director vector.
        dir = vrotate(dir, angleDiff);
    }

    mInitialAngle += DEG2RAD(mAngleVariation);
}

void C_WeaponRadial::ReceiveMessage(ComponentMessage* msg) {
    C_Weapon::ReceiveMessage(msg);
}
