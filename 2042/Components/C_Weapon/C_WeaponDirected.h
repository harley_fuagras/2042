#pragma once
#include "C_Weapon.h"

class C_WeaponDirected : public C_Weapon {
public:
    C_WeaponDirected(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner);
    virtual void Run() override;
    virtual void Shoot() override;
    virtual void ReceiveMessage(ComponentMessage* msg) override;

private:
};
