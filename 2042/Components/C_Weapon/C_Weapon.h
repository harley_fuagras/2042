#pragma once
#include "Component.h"
#include <map>

class C_Weapon : public Component {
public:
    enum Type {
        W_NONE,
        W_SIMPLE,
        W_DIRECTED,
        W_RADIAL,
        W_MULTIPLE,
    };

    C_Weapon(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner);
    virtual void Run() = 0;
    virtual void Shoot() = 0;
    virtual void ReceiveMessage(ComponentMessage* msg);
    
    static C_Weapon* Load(const std::string& weaponId, Entity* entity);
    static const std::map<std::string, Type> mWeaponTypes;

protected:
    std::string mId;
    int         mCooldown;
    int         mCurrentCooldown; // Ticks.
    float       mDamage;
    int         mChance;
    float       mBulletSpeed;
    bool        mIsActive;

};
