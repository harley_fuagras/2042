#include "stdafx.h"
#include "C_WeaponMultiple.h"
#include "Globals.h"
#include "World.h"
#include "Entity.h"

C_WeaponMultiple::C_WeaponMultiple(
    std::string id, int cooldown, int chance, float damage, float bulletSpeed,
    float angleSeparation, int bulletsNumber, Entity* owner
)
    : C_Weapon(id, cooldown, chance, damage, bulletSpeed, owner),
    mBulletsNumber(bulletsNumber), mAngleSeparation(angleSeparation)
{
}

void C_WeaponMultiple::Run() {
    C_Weapon::Run();
}

void C_WeaponMultiple::Shoot() {
    C_Weapon::Shoot();

    float initAngle = DEG2RAD((static_cast<float>((mBulletsNumber - 1)) * mAngleSeparation) / 2.f);
    vec2 dir = vrotate(vmake(0, 1), initAngle);

    for (int i = 0; i < mBulletsNumber; ++i) {
        vec2 velocity = vscale(dir, mBulletSpeed);
        gWorld->AddEntity(NewBullet(mOwner->GetPos(), velocity, mDamage, C_Faction::Type::F_ENEMY));

        // Rotate director vector.
        dir = vrotate(dir, -DEG2RAD(mAngleSeparation));
    }
}

void C_WeaponMultiple::ReceiveMessage(ComponentMessage* msg) {
    C_Weapon::ReceiveMessage(msg);
}
