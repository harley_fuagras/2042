#pragma once
#include "C_Weapon.h"

class C_WeaponSimple : public C_Weapon {
public:
    C_WeaponSimple(std::string id, int cooldown, int chance, float damage, float bulletSpeed, Entity* owner);
    virtual void Run() override;
    virtual void Shoot() override;
    virtual void ReceiveMessage(ComponentMessage* msg) override;

};
