#pragma once
#include "IC_AI.h"
#include "Component.h"

class C_AI_ZigZag : public IC_AI {
public:
    C_AI_ZigZag(Entity* owner);
    virtual void Run();

};
