#include "stdafx.h"
#include "C_SimpleMovement.h"

C_SimpleMovement::C_SimpleMovement(vec2 velocity, bool isActive, Entity* owner)
    : mVelocity(velocity), mIsActive(isActive)
{
    mOwner = owner;
}

void C_SimpleMovement::Run() {
    if (GetIsActive()) {
        vec2 newPos = vadd(mOwner->GetPos(), mVelocity);
        mOwner->SetPos(newPos);

        if (CheckOutOfBoundsDestruction(mOwner->GetType())) {
            SetDoesExplodeCMsg setDoesExplodeCMsg = { false };
            mOwner->ReceiveMessage(&setDoesExplodeCMsg);
            SetWillBeDestroyedCMsg setWillBeDestroyedMessage = { true, false };
            mOwner->ReceiveMessage(&setWillBeDestroyedMessage);
        }
    }
}

bool C_SimpleMovement::CheckOutOfBoundsDestruction(Entity::Type entityType) {
    switch (entityType) {
        //Enemies only get destroyed when they go out of screen through the bottom.
        case Entity::Type::E_ENEMY: {
            if (mOwner->GetPos().y + mOwner->GetSize().y / 2 <= 0)
                return true;
            break;
        }
        case Entity::Type::E_BULLET: {
            if (mOwner->GetPos().y + mOwner->GetSize().y / 2 <= 0 ||
                mOwner->GetPos().y - mOwner->GetSize().y / 2 >= SCR_HEIGHT ||
                mOwner->GetPos().x + mOwner->GetSize().x / 2 <= 0 ||
                mOwner->GetPos().x - mOwner->GetSize().x / 2 >= SCR_WIDTH)
                return true;
            break;
        }
        default: {
            break;
        }
    }
    return false;
}

void C_SimpleMovement::SetVelocity(vec2 velocity) {
    mVelocity = velocity;
}

vec2 C_SimpleMovement::GetVelocity() const {
    return mVelocity;
}

void C_SimpleMovement::SetIsActive(bool isActive) {
    mIsActive = isActive;
}

bool C_SimpleMovement::GetIsActive() const {
    return mIsActive;
}

void C_SimpleMovement::ReceiveMessage(ComponentMessage* msg) {
    SetVelocityCMsg *setVelocityMsg = dynamic_cast<SetVelocityCMsg*>(msg);
    if (setVelocityMsg) {
        SetVelocity(setVelocityMsg->velocity);
        return;
    }

    GetVelocityCMsg *getVelocityMsg = dynamic_cast<GetVelocityCMsg*>(msg);
    if (getVelocityMsg) {
        getVelocityMsg->velocity = GetVelocity();
        return;
    }

    SetIsActiveCMsg *setIsActiveMsg = dynamic_cast<SetIsActiveCMsg*>(msg);
    if (setIsActiveMsg) {
        SetIsActive(setIsActiveMsg->isActive);
        return;
    }

    GetIsActiveCMsg *getIsActiveMsg = dynamic_cast<GetIsActiveCMsg*>(msg);
    if (getIsActiveMsg) {
        getIsActiveMsg->isActive = GetIsActive();
        return;
    }
}
