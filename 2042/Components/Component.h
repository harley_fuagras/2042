#pragma once

class Entity;
struct ComponentMessage;

class Component {
protected:
    Entity* mOwner;

public:
    virtual void Run() {};
    virtual void ReceiveMessage(ComponentMessage*) {};
    virtual ~Component() = 0 {};

};
