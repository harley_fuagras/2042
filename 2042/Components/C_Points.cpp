#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "World.h"
#include "C_Points.h"
#include "ComponentMessage.h"

C_Points::C_Points(int points, Entity* owner)
    : mPoints(points)
{
    mOwner = owner;
}

void C_Points::Run() {
}

int C_Points::GetPoints() const {
    return mPoints;
}

void C_Points::ReceiveMessage(ComponentMessage* msg) {
    GetPointsCMsg *getPointsMsg = dynamic_cast<GetPointsCMsg*>(msg);
    if (getPointsMsg) {
        getPointsMsg->points = GetPoints();
        return;
    }
}
