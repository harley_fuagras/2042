#pragma once
#include "Component.h"

class C_OutscreenSpawn : public Component {
public:
    C_OutscreenSpawn(vec2 spawnPos, Entity* owner);
    
    void Run();
    void ActivateMovement();

private:
    vec2 mSpawnPos;
    bool mIsSpawned;

};
