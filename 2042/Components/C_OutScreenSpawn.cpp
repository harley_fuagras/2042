#include "stdafx.h"
#include "Globals.h"
#include "World.h"
#include "C_OutScreenSpawn.h"
#include "C_Collider.h"
#include "Entity.h"
#include "ComponentMessage.h"
#include "CollisionManager.h"

C_OutscreenSpawn::C_OutscreenSpawn(vec2 spawnPos, Entity* owner)
    : mSpawnPos(spawnPos)
{
    mOwner = owner;
}

void C_OutscreenSpawn::Run() {
    // Check if enemy has to spawn.
    if (!mIsSpawned && gWorld->GetCameraPos().y + SCR_HEIGHT >= mSpawnPos.y) {
        mIsSpawned = true;
        // Add collider to ColliderManager when enemy spawns to avoid unnecessary collision checks.
        GetColliderCMsg getColliderCMsg;
        mOwner->ReceiveMessage(&getColliderCMsg);
        gCollisionManager->AddCollider(getColliderCMsg.collider);
        ActivateMovement();
    }
}

void C_OutscreenSpawn::ActivateMovement() {
    SetIsActiveCMsg setIsActiveMsg{ true };
    mOwner->ReceiveMessage(&setIsActiveMsg);
}
