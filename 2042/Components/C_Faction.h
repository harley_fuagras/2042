#pragma once
#include "Component.h"

class Entity;

class C_Faction : public Component {
public:
    enum Type {
        F_PLAYER,
        F_ENEMY,
    };

    C_Faction(Type typeFaction, Entity* owner);
    
    void SetTypeFaction(Type typeFaction);
    Type GetTypeFaction() const;
    
    void ReceiveMessage(ComponentMessage* msg);

private:
    Type mTypeFaction;

};
