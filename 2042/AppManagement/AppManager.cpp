#include "stdafx.h"
#include "Globals.h"
#include "AppManager.h"
#include "AppModeMenu.h"
#include "AppModeGame.h"
#include "AppModeLevelTransition.h"

AppManager::AppManager() {
    mActiveMode   = nullptr;
    mWantedModeId = AM_NULL;
}

AppManager::~AppManager() {
    GAME_DELETE(mActiveMode);
    mActiveMode = nullptr;
};

void AppManager::Run() {
    mActiveMode->Run();
}

void AppManager::ProcessInput() {
    mActiveMode->ProcessInput();
}

void AppManager::Render() {
    mActiveMode->Render();
}

void AppManager::SwitchToMode(AppModeId modeId) {
    if (mActiveMode) {
        mActiveMode->Deactivate();
        mActiveMode->Destroy();
        mActiveMode->Unload();
    }

    // Free allocated memory for previous state.
    if (mActiveMode) {
        GAME_DELETE(mActiveMode);
        mActiveMode = nullptr;
    }

    switch (modeId) {
        case AM_NULL:
            mActiveMode = nullptr;
            break;
        case AM_MENU:
            mActiveMode = GAME_NEW(AppModeMenu, ());
            break;
        case AM_GAME:
            mActiveMode = GAME_NEW(AppModeGame, ());
            break;
        case AM_LEVEL_TRANSITION:
            mActiveMode = GAME_NEW(AppModeLevelTransition, ());
            break;
        default:
            mActiveMode = nullptr;
            break;
    }

    if (mActiveMode) {
        mActiveMode->Init();
        mActiveMode->Load();
        mActiveMode->Activate();
    }
}

AppMode* AppManager::GetActiveMode() const {
    return mActiveMode;
}

AppManager::AppModeId AppManager::GetWantedModeId() const {
    return mWantedModeId;
}

std::string AppManager::GetWantedOptionId() const {
    return mWantedOptionId;
}

void AppManager::ManageModes() {
    if (mActiveMode->GetId() != mWantedModeId) {
        SwitchToMode(mWantedModeId);
        mWantedModeId = mActiveMode->GetId();
    }
}

void AppManager::SetWantedModeId(AppModeId modeId) {
    mWantedModeId = modeId;
}

void AppManager::SetWantedOptionId(std::string& wantedOptionId) {
    mWantedOptionId = wantedOptionId;
}
