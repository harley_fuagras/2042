#include "stdafx.h"
#include "Globals.h"
#include "Game.h"
#include "AppModeMenu.h"
#include "TextureManager.h"
#include "Menu.h"
#include "Background.h"
#include "GraphicEngine.h"

void AppModeMenu::Init() {
    GAME_NEW(Menu, (Menu::MS_MAIN_MENU));
}

void AppModeMenu::Load() {
    std::string menuBgTexPath = MENU_BG_TEX_PATH;
    Background* menuBg        = GAME_NEW(Background, (vmake(SCR_WIDTH, SCR_HEIGHT), vmake(SCR_WIDTH / 2, SCR_HEIGHT / 2), menuBgTexPath, vmake(0, 0)));
    gGraphicEngine->AddBackground(menuBg);

    std::string music    = MUSIC_MENU_PATH;
    std::string musicKey = MUSIC_MENU_KEY;
    gSoundManager->LoadSound(music, musicKey);

    std::string selectFx    = FX_SELECT_PATH;
    std::string selectFxKey = FX_SELECT_KEY;
    gSoundManager->LoadSound(selectFx, selectFxKey);
}

void AppModeMenu::Activate() {
    std::string musicKey = MUSIC_MENU_KEY;
    gSoundManager->PlayMusic(musicKey);
}

void AppModeMenu::Deactivate() {
    gSoundManager->StopMusic();
}

void AppModeMenu::Destroy() {
    gGraphicEngine->Clear();
}

void AppModeMenu::Unload() {
}

void AppModeMenu::Run() {
}

void AppModeMenu::ProcessInput() {
    gEventManager->ManageInput();
}

void AppModeMenu::Render() {
    gGraphicEngine->Render();
}

GLuint AppModeMenu::GetTexMenuBg() const {
    return mTexMenuBg;
}

void AppModeMenu::SetTexMenuBg(GLuint tex) {
    mTexMenuBg = tex;
}

AppManager::AppModeId AppModeMenu::GetId() const {
    return AppManager::AM_MENU;
}
