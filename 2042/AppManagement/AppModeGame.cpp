#include "stdafx.h"
#include "Game.h"
#include "ComponentMessage.h"
#include "AppModeGame.h"
#include "Globals.h"
#include "World.h"
#include "Background.h"
#include "GameUtils.h"
#include "EventManager.h"
#include "CollisionManager.h"
#include "SoundManager.h"
#include "GraphicEngine.h"
#include "Entity.h"
#include <vector>

void AppModeGame::Init() {
    if (!gWorld)
        gWorld = GAME_NEW(World, ());
    else 
        gWorld->ResetLevel();
}

void AppModeGame::Load() {
    std::string levelId = gAppManager->GetWantedOptionId();
    LoadLevel(levelId);
    
    std::string music    = MUSIC_GAME_PATH;
    std::string musicKey = MUSIC_GAME_KEY;
    gSoundManager->LoadSound(music, musicKey);

    std::string laserFxPath = FX_LASER_PATH;
    std::string laserFxKey  = FX_LASER_KEY;
    ALuint laserFx          = gSoundManager->LoadSound(laserFxPath, laserFxKey);

    std::string explosionFxPath = FX_EXPLOSION_PATH;
    std::string explosionFxKey  = FX_EXPLOSION_KEY;
    ALuint explosionFx          = gSoundManager->LoadSound(explosionFxPath, explosionFxKey);

    std::string hurtFxPath = FX_HURT_PATH;
    std::string hurtFxKey  = FX_HURT_KEY;
    ALuint hurtFx = gSoundManager->LoadSound(hurtFxPath, hurtFxKey);
}

void AppModeGame::Activate() {
    std::string musicKey = MUSIC_GAME_KEY;
    gSoundManager->PlayMusic(musicKey);
}

void AppModeGame::Deactivate() {
    gSoundManager->StopMusic();
}

void AppModeGame::Destroy() {
    // If we go from AppModeGame to AppModeLevelTransition we don't want to destroy the world (as it may contain the player and more info needed).
    // However we do want to destroy it when we go elsewhere.
    if (gWorld->GetWillChangeLevel() || gWorld->GetWillRestartLevel()) {
        Entity* player = gWorld->GetPlayer();

        for (auto& entity : gWorld->GetEntities()) {
            // Hide entities & avoid explosions.
            GetSpriteCMsg getSpriteMsg;
            entity->ReceiveMessage(&getSpriteMsg);
            getSpriteMsg.sprite->SetIsVisible(false);

            SetDoesExplodeCMsg setDoesExplodeMsg { false };
            entity->ReceiveMessage(&setDoesExplodeMsg);
            
            if (entity != player) {
                gWorld->AddEntityToDestroy(entity);
            }
        }
    } else {
        GAME_DELETE(gWorld);
        gWorld = nullptr;
        gGraphicEngine->Clear();
    }
}

void AppModeGame::Unload() {
}

void AppModeGame::Run() {
    gWorld->Run();
}

void AppModeGame::ProcessInput() {
    gEventManager->ManageInput();
}

void AppModeGame::Render() {
    gGraphicEngine->Render();
}

AppManager::AppModeId AppModeGame::GetId() const {
    return AppManager::AM_GAME;
}

void AppModeGame::LoadLevel(std::string& levelName) const {
    std::map<std::string, Game::LevelData> levelsData = gGame->GetLevelsData();
    std::map<std::string, Game::LevelData>::iterator levelIt = levelsData.find(levelName);

    CUSTOM_ASSERT(levelIt != levelsData.end(), "Level not found");
    
    gWorld->SetCurrentLevelName((*levelIt).second.id);
    gWorld->SetMapSize((*levelIt).second.size);
    gWorld->SetCameraVelocity((*levelIt).second.cameraVelocity);

    gAppManager->SetWantedOptionId((*levelIt).second.nextLevel);

    for (auto bgIt = begin((*levelIt).second.bgLevelData); bgIt != end((*levelIt).second.bgLevelData); ++bgIt) {
        Background* bg = GAME_NEW(Background, ((*bgIt).size, (*bgIt).pos, (*bgIt).texPath, (*bgIt).scrollRatio));
        gWorld->AddBackground(bg);
    }

    for (auto enemyIt = begin((*levelIt).second.enemyLevelData); enemyIt != end((*levelIt).second.enemyLevelData); ++enemyIt) {
        std::string enemyId = (*enemyIt).id;
        vec2 initPos        = (*enemyIt).initPos;
        vec2 spawnPos       = (*enemyIt).spawnPos;
        LoadEnemy(enemyId, initPos, spawnPos);
    }
}

void AppModeGame::LoadEnemy(std::string& enemyId, vec2 initPos, vec2 spawnPos) const {
    std::map<std::string, Game::EnemyData> enemiesData = gGame->GetEnemiesData();
    std::map<std::string, Game::EnemyData>::iterator enemyIterator = enemiesData.find(enemyId);

    CUSTOM_ASSERT(enemyIterator != enemiesData.end(), "Enemy not found");

    vec2 size                                     = (*enemyIterator).second.size;
    vec2 velocity                                 = (*enemyIterator).second.velocity;
    std::vector<std::vector<std::string>> texPath = (*enemyIterator).second.texPaths;
    std::string aiId                              = (*enemyIterator).second.aiId;
    std::string stateMachinePath                  = (*enemyIterator).second.stateMachinePath;
    std::vector<std::string> weaponIds            = (*enemyIterator).second.weaponIds;
    int points                                    = (*enemyIterator).second.points;
    float health                                  = (*enemyIterator).second.health;
    float collisionDamage                         = (*enemyIterator).second.collisionDamage;

    gWorld->AddEntity(NewEnemy(
        initPos, spawnPos, size, velocity, texPath, aiId,
        stateMachinePath, weaponIds, points, health, collisionDamage
    ));
}
