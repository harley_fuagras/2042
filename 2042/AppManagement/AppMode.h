#pragma once
#include "AppManager.h"
#include "SoundManager.h"

class Menu;

class AppMode {
public:
    virtual void Init() = 0;
    virtual void Load() = 0;
    virtual void Activate() = 0;
    
    virtual void Deactivate() = 0;
    virtual void Destroy() = 0;
    virtual void Unload() = 0;

    virtual void Run() = 0;
    virtual void ProcessInput() = 0;
    virtual void Render() = 0;

    virtual AppManager::AppModeId GetId() const = 0;

protected:
    Menu*  mMenuState;

};
