#include "stdafx.h"
#include "Globals.h"
#include "AppManager.h"
#include "AppModeLevelTransition.h"
#include "Game.h"
#include "World.h"
#include "GraphicEngine.h"
#include "Sprite.h"
#include "Background.h"
#include "TextBox.h"
#include "ComponentMessage.h"
#include "LocalizationManager.h"

void AppModeLevelTransition::Init() {
    mTransitionTicksLeft = L_TRANS_TICKS;
}

void AppModeLevelTransition::Load() {
    std::string transText = gLocalizationManager->GetString("entering");
    float posX = GetLineHCenterPos(SCR_WIDTH, transText.length(), L_TRANS_CHAR_WIDTH);
    float posY = L_TRANS_POS_Y;
    mTextBoxTop = GAME_NEW(TextBox, (transText, L_TRANS_TEXT_COLOR, vmake(posX, posY)));
    gGraphicEngine->AddTextBox(mTextBoxTop);

    // Get level name translated.
    std::map<std::string, Game::LevelData> levelsData = gGame->GetLevelsData();
    std::map<std::string, Game::LevelData>::iterator levelIt = levelsData.find(gAppManager->GetWantedOptionId());
    std::string levelName = "";
    if (levelIt != levelsData.end()) {
        levelName = gLocalizationManager->GetString((*levelIt).second.levelName);
    }
    
    posX = GetLineHCenterPos(SCR_WIDTH, levelName.length(), L_TRANS_CHAR_WIDTH);
    posY -= L_TRANS_MARGIN_Y;
    mTextBoxBottom = GAME_NEW(TextBox, (levelName, L_TRANS_TEXT_COLOR, vmake(posX, posY)));
    gGraphicEngine->AddTextBox(mTextBoxBottom);
    
    std::string menuBgTexPath = L_TRANS_BG_PATH;
    Background* background = GAME_NEW(Background, (vmake(SCR_WIDTH, SCR_HEIGHT), vmake(SCR_WIDTH / 2, SCR_HEIGHT / 2), menuBgTexPath, vmake(0, 0)));
    gGraphicEngine->AddBackground(background);
}

void AppModeLevelTransition::Activate() {
}

void AppModeLevelTransition::Deactivate() {
}

void AppModeLevelTransition::Destroy() {
    gGraphicEngine->RemoveTextBox(mTextBoxTop);
    gGraphicEngine->RemoveTextBox(mTextBoxBottom);

    GAME_DELETE(mTextBoxTop);
    mTextBoxTop = nullptr;
    GAME_DELETE(mTextBoxBottom);
    mTextBoxBottom = nullptr;
}

void AppModeLevelTransition::Unload() {
}

void AppModeLevelTransition::Run() {
    mTransitionTicksLeft--;
    if (mTransitionTicksLeft <= 0) {
        gAppManager->SetWantedModeId(AppManager::AM_GAME);
    }
}

void AppModeLevelTransition::ProcessInput() {
}

void AppModeLevelTransition::Render() {
    gGraphicEngine->Render();
}

AppManager::AppModeId AppModeLevelTransition::GetId() const {
    return AppManager::AM_LEVEL_TRANSITION;
}
