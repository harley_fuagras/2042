#pragma once
#include "AppMode.h"
#include "AppManager.h"

class TextBox;

class AppModeLevelTransition : public AppMode {
public:
    void Init();
    void Load();
    void Activate();
    void Deactivate();
    void Destroy();
    void Unload();
    void Run();
    void ProcessInput();
    void Render();

    AppManager::AppModeId GetId() const;

private:
    int      mTransitionTicksLeft;
    TextBox* mTextBoxTop;
    TextBox* mTextBoxBottom;

};
