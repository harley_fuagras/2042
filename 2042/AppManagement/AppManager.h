#pragma once

class AppMode;

class AppManager {
public:
    enum AppModeId {
        AM_NULL,
        AM_MENU,
        AM_GAME,
        AM_LEVEL_TRANSITION,
    };

    AppManager();
    ~AppManager();
    void        Run();
    void        ProcessInput();
    void        Render();
    void        SwitchToMode(AppModeId modeId);
    AppMode*    GetActiveMode() const;
    AppModeId   GetWantedModeId() const;
    std::string GetWantedOptionId() const;
    void        ManageModes();
    void        SetWantedModeId(AppModeId modeId);
    void        SetWantedOptionId(std::string& wantedOptionId);

private:
    AppMode*        mActiveMode;
    AppModeId       mWantedModeId;
    std::string     mWantedOptionId;

};
