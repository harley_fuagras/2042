#pragma once
#include "AppMode.h"

class Menu;
class AppManager;

class AppModeMenu : public AppMode {
public:
    void Init();
    void Load();
    void Activate();
    void Deactivate();
    void Destroy();
    void Unload();
    void Run();
    void ProcessInput();
    void Render();

    GLuint GetTexMenuBg() const;
    void   SetTexMenuBg(GLuint tex);

    AppManager::AppModeId GetId() const;

private:
    GLuint mTexMenuBg;

};
