#pragma once
#include "AppMode.h"
#include "AppManager.h"

class  World;
struct vec2;

class AppModeGame : public AppMode {
public:
    void Init();
    void Load();
    void Activate();
    void Deactivate();
    void Destroy();
    void Unload();
    void Run();
    void ProcessInput();
    void Render();
    void LoadLevel(std::string& levelName) const;
    void LoadEnemy(std::string& enemyId, vec2 initPos, vec2 spawnPos) const;

    AppManager::AppModeId GetId() const;

};
